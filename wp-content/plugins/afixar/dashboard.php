<?php
/**
Plugin Name: Afixar Dashboard 
Plugin URI: http://afixar.com.br
Description: Suporte Dashboard 
Version: 1.0
Author: Afixar
Author URI: www.afixar.com.br
Text Domain: afixar_wp
*/
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');
 
function my_custom_dashboard_widgets() {
    global $wp_meta_boxes;
    //wp_add_dashboard_widget
    add_meta_box('custom_help_widget', 'Suporte', 'custom_dashboard_help','dashboard', 'side', 'high');
    
    add_meta_box('custom_notification_widget', 'Notificações e Avisos', 'custom_dashboard_notification','dashboard', 'side', 'high');
}

function custom_dashboard_help() {
echo '';
}

function custom_dashboard_notification() {
echo '';
}