<?php
/* Template Name: Página Contato */
get_header();

$id_home = get_id_by_slug('home');
$id_contato = get_id_by_slug('contato');

?>


<!-- Titlebar
================================================== -->
<div id="titlebar">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <h2><?php echo get_field('contato_titulo_pagina', $id_contato); ?></h2>

                <!-- Breadcrumbs
                <nav id="breadcrumbs">
                        <ul>
                                <li><a href="#">Home</a></li>
                                <li>Typography</li>
                        </ul>
                </nav> -->

            </div>
        </div>
    </div>
</div>


<!-- Container / Start -->
<div class="container">

	<div class="row">

		<!-- Contact Details -->
		<div class="col-md-4">

			<h4 class="headline margin-bottom-30"><?php echo get_field('contato_titulo_descricao', $id_contato); ?></h4>

			<!-- Contact Details -->
			<div class="sidebar-textbox">
				<p><?php echo get_field('contato_descricao', $id_contato); ?></p>

				<ul class="contact-details margin-top-40">
					<li class="margin-bottom-30"><i class="im im-icon-Location-2"></i> <strong>Endereço:</strong> <span><?=get_field('home_onde_estamos', $id_home)?></span></li>
					<li class="margin-bottom-30"><i class="im im-icon-Phone"></i> <strong>Telefone:</strong> <span><?=get_field('home_telefone', $id_home)?></span></li>
                                        
                                        <?php if (get_field('home_whatsapp', $id_home)):?>
                                            <li class="margin-bottom-30"><i class="im im-icon-Phone-2"></i> <strong>WhatsApp:</strong> <span><?=get_field('home_whatsapp', $id_home)?></span></li>
                                        <?php endif;?>
				</ul>
			</div>

		</div>

		<!-- Contact Form -->
		<div class="col-md-8 margin-bottom-90">

			<section id="contact">
                            
				<h4 class="headline margin-bottom-35"><?php echo get_field('contato_titulo_formulario', $id_contato); ?></h4>

				<div id="contact-message"></div>

                                <form method="post" action="<?php echo get_template_directory_uri(); ?>/contact.php" name="contactform" id="contactform" autocomplete="on">

					<div class="row">
						<div class="col-md-6">
							<div>
								<input name="name" type="text" id="name" placeholder="Seu nome" required="required" />
							</div>
						</div>

						<div class="col-md-6">
							<div>
								<input name="email" type="email" id="email" placeholder="Seu e-mail" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" required="required" />
							</div>
						</div>
					</div>

					<div>
						<input name="subject" type="text" id="subject" placeholder="Assunto" required="required" />
					</div>

					<div>
						<textarea name="comments" cols="40" rows="3" id="comments" placeholder="Mensagem" spellcheck="true" required="required"></textarea>
					</div>

					<input type="submit" class="submit button" id="submit" value="Enviar mensagem" loading="<?php echo get_template_directory_uri().'/images/loader.gif'; ?>"/>

					</form>
			</section>
		</div>
		<!-- Contact Form / End -->

	</div>

</div>
<!-- Container / End -->


<?php get_footer();?>
