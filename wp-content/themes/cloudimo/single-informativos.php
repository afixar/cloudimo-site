<?php
get_header();
?>

<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Infomativo</h2>
				<!--span>Keep up to date with the latest news</span-->
				
				<!-- Breadcrumbs
				<nav id="breadcrumbs">
					<ul>
						<li><a href="#">Home</a></li>
						<li>Blog</li>
					</ul>
				</nav>
                                -->

			</div>
		</div>
	</div>
</div>



<!-- Content
================================================== -->
<div class="container">

    <!-- Blog Posts -->
    <div class="blog-page">
        <div class="row">


            <!-- Post Content -->
            <div class="col-md-8">


                <!-- Blog Post -->
                <div class="blog-post single-post">

                    <!-- Img -->
                    <img class="post-img" src="<?php the_post_thumbnail_url( 'large' ); ?>" alt="imagem do post">

                    <!-- Content -->
                    <div class="post-content">
                        <h3><?php the_title(); ?></h3>

                        <!--ul class="post-meta">
                            <li>Novemer 9, 2016</li>
                            <li><a href="#">5 Comments</a></li>
                        </ul-->
                        <?php echo get_field('informativo_texto');?>


                        <?php
                        include (locate_template('compartilhar-botoes.php'));
                        ?> 
                        
                        <div class="clearfix"></div>

                    </div>
                </div>
                <!-- Blog Post / End -->


                <!-- Post Navigation --
                <ul id="posts-nav" class="margin-top-0 margin-bottom-40">
                    <li class="next-post">
                        <a href="#"><span>Próximo</span>
                            Tips For Newbie Hitchhiker</a>
                    </li>
                    <li class="prev-post">
                        <a href="#"><span>Anterior</span>
                            What's So Great About Merry?</a>
                    </li>
                </ul>-->


                <!-- About Author
                <div class="about-author">
                    <img src="images/agent-avatar.jpg" alt="" />
                    <div class="about-description">
                        <h4>Jennie Wilson</h4>
                        <a href="#">jennie@example.com</a>
                        <p>Nullam ultricies, velit ut varius molestie, ante metus condimentum nisi, dignissim facilisis turpis ex in libero. Sed porta ante tortor, a pulvinar mi facilisis nec. Proin finibus dolor ac convallis congue.</p>
                    </div>
                </div>-->


            </div>
            <!-- Content / End -->



            <!-- Sidebar
            ================================================== -->

            <!-- Widgets -->
            <div class="col-md-4">
                <div class="sidebar right">

                    <!-- Widget --
                    <div class="widget">
                        <h3 class="margin-top-0 margin-bottom-25">Procurar</h3>
                        <div class="search-blog-input">
                            <div class="input"><input class="search-field" type="text" placeholder="Digite sua pesquisa" value=""/></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    < !-- Widget / End -->


                    <!-- Widget -->
                    <div class="widget">
                        <h3>Tem alguma dúvida?</h3>
                        <div class="info-box margin-bottom-10">
                            <p>Se você tiver alguma dúvida, por favor entre em contato.</p>
                            <a href="<?php echo get_permalink( get_id_by_slug('contato') ); ?>" class="button fullwidth margin-top-20"><i class="fa fa-envelope-o"></i> Contate-nos</a>
                        </div>
                    </div>
                    <!-- Widget / End -->


                    <!-- Widget -->
                    <div class="widget">

                        <h3>Outros informativos</h3>
                        <ul class="widget-tabs">
                            <?php
                            
                            $args = array(
                                'post_type'         => 'informativos',
                                'orderby'           => 'meta_value',
                                'order'             => 'DESC',
                                'posts_per_page'    => 3,
                            );

                            $informativos = new WP_Query($args);
                            
                            while ($informativos->have_posts()) : $informativos->the_post();
                            ?>
                            <!-- Post #1 -->
                            <li>
                                <div class="widget-content">
                                    <div class="widget-thumb">
                                        <a href="<?php the_permalink();?>"><img src="<?php the_post_thumbnail_url( 'informativos' ); ?>" alt=""></a>
                                    </div>

                                    <div class="widget-text">
                                        <h5><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h5>
                                        <!--span>October 26, 2016</span-->
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </li>
                            <?php endwhile; ?>
                            

                        </ul>

                    </div>
                    <!-- Widget / End-->


                    <?php
                    
                    $id_home      = get_id_by_slug('home');                    
                    $facebook     = get_field('home_facebook', $id_home);
                    $twitter      = get_field('home_twitter', $id_home);
                    $google_plus  = get_field('home_google_plus', $id_home);
                    $pinterest    = get_field('home_pinterest', $id_home);
                    
                    ?>
                    <!-- Widget -->
                    <div class="widget">
                        <h3 class="margin-top-0 margin-bottom-25">Social</h3>
                        <ul class="social-icons rounded">
                            <?php if ($facebook): ?>
                            <li><a class="facebook" href="<?=$facebook?>"><i class="icon-facebook"></i></a></li>
                            <?php endif; ?>
                            <?php if ($twitter): ?>
                            <li><a class="twitter" href="<?=$twitter?>"><i class="icon-twitter"></i></a></li>
                            <?php endif; ?>
                            <?php if ($google_plus): ?>
                            <li><a class="gplus" href="<?=$google_plus?>"><i class="icon-gplus"></i></a></li>
                            <?php endif; ?>
                            <?php if ($pinterest): ?>
                            <li><a class="pinterest" href="<?=$pinterest?>"><i class="icon-pinterest"></i></a></li>
                            <?php endif; ?>                            
                        </ul>

                    </div>
                    <!-- Widget / End-->

                    <div class="clearfix"></div>
                    <div class="margin-bottom-40"></div>
                </div>
            </div>
        </div>
        <!-- Sidebar / End -->


    </div>
</div>

<?php get_footer(); ?>
