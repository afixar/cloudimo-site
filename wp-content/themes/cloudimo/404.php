<?php
    get_header();
?>

<div id="titlebar">
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="text-align: center;">
                <h1>404</h1>
                <h4>Ops! A página que você procura não foi encontrada.</h4>
            </div>
        </div>
    </div>
</div>

<?php
    get_footer();
?>

