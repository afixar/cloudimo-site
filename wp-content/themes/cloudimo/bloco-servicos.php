<?php
$args = array(
    'post_type'         => 'servicos',
    'orderby'           => 'meta_value',
    'order'             => 'ASC',
    'posts_per_page'    => 4,
);

$servicos = new WP_Query($args);

$id_home = get_id_by_slug('home');

if($servicos->have_posts())
{
?>
<!-- Fullwidth Section -->
<section class="fullwidth margin-top-105" data-background-color="#f7f7f7">

    <!-- Box Headline -->
    <h3 class="headline-box"><?php echo get_field('home_titulo_servicos', $id_home); ?></h3>

    <!-- Content -->
    <div class="container">
        <div class="row">

            <?php
            while ($servicos->have_posts()) : $servicos->the_post();?>
                
            
            
                <div class="col-md-3 col-sm-6">
                    <!-- Icon Box -->
                    <div class="icon-box-1">

                        <div class="icon-container">
                                <i class="<?php echo get_field('servico_icone');?>"></i>
                                <div class="icon-links">
                                    <a href="<?php
                                    if (get_field('servico_link')) {
                                        echo get_field('servico_link');
                                    } else {
                                        the_permalink();
                                    }
                                    ?>">Saiba mais</a>
                                </div>
                        </div>

                        <h3><?php echo get_field('servico_titulo');?></h3>
                        <p><?php echo get_field('servico_descricao');?></p>
                        
                    </div>
                </div>           
                
                
            <?php endwhile; ?>

        
        </div>
    </div>
</section>
<!-- Fullwidth Section / End -->
<?php
}
?>