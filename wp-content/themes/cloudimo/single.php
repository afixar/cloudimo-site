<?php
get_header();
?>

<!-- Titlebar
================================================== -->
<div id="titlebar">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Blog</h2>
            </div>
        </div>
    </div>
</div>



<!-- Content
================================================== -->
<div class="container">

    <!-- Blog Posts -->
    <div class="blog-page">
        <div class="row">


            <!-- Post Content -->
            <div class="col-md-8">


                <!-- Blog Post -->
                <div class="blog-post single-post">

                    <!-- Img -->
                    <?php if (has_post_thumbnail()) :?>
                    <img class="post-img" src="<?php the_post_thumbnail_url('large'); ?>" alt="imagem do post">
                    <?php endif; ?>
                    
                    <!-- Content -->
                    <div class="post-content">
                        <h3><?php the_title(); ?></h3>

                        
                        <?php
			// Start the Loop.
			while ( have_posts() ) : the_post();
                            the_content();
			// End the loop.
			endwhile;			
                        ?>
                        

                        <!-- Share Buttons -->
                        <ul class="share-buttons margin-top-40 margin-bottom-0">
                            <li><a class="fb-share" href="#"><i class="fa fa-facebook"></i> Share</a></li>
                            <li><a class="twitter-share" href="#"><i class="fa fa-twitter"></i> Tweet</a></li>
                            <li><a class="gplus-share" href="#"><i class="fa fa-google-plus"></i> Share</a></li>
                            <li><a class="pinterest-share" href="#"><i class="fa fa-pinterest-p"></i> Pin</a></li>
                        </ul>
                        <div class="clearfix"></div>

                    </div>
                </div>
                <!-- Blog Post / End -->

            </div>
            <!-- Content / End -->



            <!-- Sidebar
            ================================================== -->

            <!-- Widgets -->
            <div class="col-md-4">
                <div class="sidebar right">

                    <!-- Widget --
                    <div class="widget">
                        <h3 class="margin-top-0 margin-bottom-25">Procurar</h3>
                        <div class="search-blog-input">
                            <div class="input"><input class="search-field" type="text" placeholder="Digite sua pesquisa" value=""/></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    < !-- Widget / End -->


                    <!-- Widget -->
                    <div class="widget">

                        <h3>Outros Artigos</h3>
                        <ul class="widget-tabs">
                            <?php
                            
                            $args = array(
                                'post_type'         => 'post',
                                'orderby'           => 'meta_value',
                                'order'             => 'DESC',
                                'posts_per_page'    => 10,
                            );

                            $servicos = new WP_Query($args);
                            
                            while ($servicos->have_posts()) : $servicos->the_post();
                            ?>
                            <!-- Post #1 -->
                            <li>
                                <div class="widget-content">
                                    <div class="widget-thumb">
                                        <a href="<?php the_permalink();?>">
                                            <img src="<?php
                                            if (has_post_thumbnail()) {
                                                the_post_thumbnail_url();
                                            } else {
                                                echo get_template_directory_uri() . '/images/no-image-thumb.png';
                                            }                                            
                                            ?>" alt="imagem do post">
                                        </a>
                                    </div>

                                    <div class="widget-text">
                                        <h5><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h5>
                                        <!--span>October 26, 2016</span-->
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </li>
                            <?php endwhile; ?>
                            

                        </ul>

                    </div>
                    <!-- Widget / End-->


                    <?php
                    
                    $id_home      = get_id_by_slug('home');                    
                    $facebook     = get_field('home_facebook', $id_home);
                    $twitter      = get_field('home_twitter', $id_home);
                    $google_plus  = get_field('home_google_plus', $id_home);
                    $pinterest    = get_field('home_pinterest', $id_home);
                    
                    ?>
                    <!-- Widget -->
                    <div class="widget">
                        <h3 class="margin-top-0 margin-bottom-25">Social</h3>
                        <ul class="social-icons rounded">
                            <?php if ($facebook): ?>
                            <li><a class="facebook" href="<?=$facebook?>"><i class="icon-facebook"></i></a></li>
                            <?php endif; ?>
                            <?php if ($twitter): ?>
                            <li><a class="twitter" href="<?=$twitter?>"><i class="icon-twitter"></i></a></li>
                            <?php endif; ?>
                            <?php if ($google_plus): ?>
                            <li><a class="gplus" href="<?=$google_plus?>"><i class="icon-gplus"></i></a></li>
                            <?php endif; ?>
                            <?php if ($pinterest): ?>
                            <li><a class="pinterest" href="<?=$pinterest?>"><i class="icon-pinterest"></i></a></li>
                            <?php endif; ?>                            
                        </ul>

                    </div>
                    <!-- Widget / End-->

                    <div class="clearfix"></div>
                    <div class="margin-bottom-40"></div>
                </div>
            </div>
        </div>
        <!-- Sidebar / End -->


    </div>
</div>

<?php
get_footer();
?>
