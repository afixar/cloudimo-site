<?php

$id_home = get_id_by_slug('home');
$logo_home = get_field('home_logo', $id_home);

//Capturando email para contato
$email = get_bloginfo('admin_email');
if (get_field('home_email', $id_home)) {
    $email = get_field('home_email', $id_home);
}


if (!session_id()) {
    session_start();
}

$nome   = get_bloginfo('name');
$titulo = $nome;
$sobre  = resumir(get_bloginfo('description'), 300, true);
$post   = get_post();
$id     = $post->ID;

if ((get_the_post_thumbnail_url($id, array(600,600))) == '') {
    $img = $logo_home['sizes']['home_logo'];
} else {
    $img = get_the_post_thumbnail_url($id, array(600,600));
}

if (! is_page('home')) {
    if ($post->post_title) $titulo = $nome.' - '.$post->post_title;
    else $titulo = $nome;
    
    $sobre = resumir(wp_strip_all_tags($post->post_content), 300, true);
}
?>
<!DOCTYPE html>
<!--
     ****************************************************************************
     * Desenvolvido por: Afixar - Desenvolvimento Web e Design                  *
     * Conteúdo protegido pela lei Nº 9.610/98 de Direitos Autorais.            *
     * É expressamente proibida a cópia ou reprodução sem autorização.          *
     ****************************************************************************
-->
<html>
    <head>        
        
        <title><?=$titulo?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <meta name="description" content="<?=$sobre?>">
        <meta name="author" content="<?=$nome?>">
        <meta name="Googlebot" content="index, follow" />
        <meta name="Robots" content="all, follow" />
        <meta name="Copyright" content="© <?=date('Y')?> AFIXAR - Desenvolvimento Web e Design LTDA">

        <!-- fb:start -->
        <meta property="og:title" content="<?=$titulo?>" />
        <meta property="og:url" content="<?=the_permalink($id)?>" />
        <meta property='og:type' content="article" />
        <meta property="og:description" content="<?=$sobre?>" />
        <meta property="og:image:url" content="<?=$img?>" />
        <meta property="og:image" content="<?=$img?>" />
        <!-- fb:end -->

        
        <!-- Styles -->
        <link rel="stylesheet" href="<?=get_template_directory_uri()?>/css/style.css">
        
        <?php $color = get_field('home_cor_tema', $id_home); if ($color): ?>
        <link rel="stylesheet" href="<?=get_template_directory_uri()?>/css/colors/<?=$color?>.css" id="colors">
        <?php endif; ?>
        
        
        <link rel="stylesheet" href="<?=get_template_directory_uri()?>/style.css">
        <script type="text/javascript" src="<?=get_template_directory_uri()?>/scripts/jquery-2.2.0.min.js"></script>

        
        <!-- Favicons -->
        <?php
        $image_fav = get_field('home_favicon', $id_home);
        ?>
        
        <link rel="icon" href="<?php echo $image_fav['sizes'][ 'favicon' ]; ?>">
        <link rel="apple-touch-icon" href="<?php echo $image_fav['sizes'][ 'favicon57' ]; ?>">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $image_fav['sizes'][ 'favicon72' ]; ?>">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $image_fav['sizes'][ 'favicon114' ]; ?>">
        
        
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
        <!--[if lt IE 9]>
        <script src="<?php echo get_template_directory_uri(); ?>/scripts/html5shiv.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/scripts/respond.min.js"></script>
        <![endif]-->
        <!--headerIncludes-->
        
        
    </head>

<body>

    <!-- Wrapper finish end footer -->
    <div id="wrapper">


    <!-- Header Container
    ================================================== -->
    <header id="header-container">

            <!-- Topbar -->
            <div id="top-bar">
                    <div class="container">

                            <!-- Left Side Content -->
                            <div class="left-side">

                                    <!-- Top bar -->
                                    <ul class="top-bar-menu">
                                            <li><i class="fa fa-phone"></i><?=get_field('home_telefone', $id_home)?></li>
                                            <li><i class="fa fa-envelope"></i> <a href="#"><?=$email?></a></li>
                                            <!--<li>
                                                    <div class="top-bar-dropdown">
                                                            <span>Dropdown Menu</span>
                                                            <ul class="options">
                                                                    <li><div class="arrow"></div></li>
                                                                    <li><a href="#">Nice First Link</a></li>
                                                                    <li><a href="#">Second Link With Long Title</a></li>
                                                                    <li><a href="#">Third Link</a></li>
                                                            </ul>
                                                    </div>
                                            </li>-->
                                    </ul>

                            </div>
                            <!-- Left Side Content / End -->


                            <!-- Left Side Content -->
                            <div class="right-side">

                                    <!-- Social Icons -->
                                    <?php
                                    $facebook     = get_field('home_facebook', $id_home);
                                    $twitter      = get_field('home_twitter', $id_home);
                                    $google_plus  = get_field('home_google_plus', $id_home);
                                    $pinterest    = get_field('home_pinterest', $id_home);
                                    
                                    ?>
                                    <ul class="social-icons">
                                            <?php if ($facebook): ?>
                                            <li><a class="facebook" href="<?=$facebook?>"><i class="icon-facebook"></i></a></li>
                                            <?php endif; ?>
                                            <?php if ($twitter): ?>
                                            <li><a class="twitter" href="<?=$twitter?>"><i class="icon-twitter"></i></a></li>
                                            <?php endif; ?>
                                            <?php if ($google_plus): ?>
                                            <li><a class="gplus" href="<?=$google_plus?>"><i class="icon-gplus"></i></a></li>
                                            <?php endif; ?>
                                            <?php if ($pinterest): ?>
                                            <li><a class="pinterest" href="<?=$pinterest?>"><i class="icon-pinterest"></i></a></li>
                                            <?php endif; ?>
                                    </ul>

                            </div>
                            <!-- Left Side Content / End -->

                    </div>
            </div>
            <div class="clearfix"></div>
            <!-- Topbar / End -->


            <!-- Header -->
            <div id="header">
                    <div class="container">

                            <!-- Left Side Content -->
                            <div class="left-side">

                                    <!-- Logo -->
                                    <div id="logo">
                                            <a href="<?php echo get_home_url(); ?>/"><img src="<?php echo $logo_home['sizes']['home_logo']; ?>" alt="<?=get_bloginfo('name')?>"></a>
                                    </div>

                </div>
                            <!-- Left Side Content / End -->

                            <!-- Right Side Content / End -->
                            <div class="right-side">


                    <!-- Mobile Navigation -->
                                    <div class="menu-responsive">
                                            <i class="fa fa-reorder menu-trigger"></i>
                                    </div>


                                    <!-- Main Navigation -->


                                            <?php
                                                $defaults = array(
                                                    'theme_location'  => 'primary',
                                                    'menu'            => '', 
                                                    'container'       => 'nav', 
                                                    'container_class' => 'style-1', 
                                                    'container_id'    => 'navigation',
                                                    'menu_class'      => 'y', 
                                                    'menu_id'         => 'responsive',
                                                    'echo'            => true,
                                                    'fallback_cb'     => 'wp_page_menu',
                                                    'before'          => '',
                                                    'after'           => '',
                                                    'link_before'     => '',
                                                    'link_after'      => '',
                                                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                                    'depth'           => 0,
                                                    'walker'          => ''
                                                );
                                                wp_nav_menu($defaults);                                            
                                            ?>

                                            <?php
                      /*  echo '<pre>';
                                              $menu_name = 'primary';
                                              $locations = get_nav_menu_locations();
                                              $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
                                              $menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );
                                              print_r($menuitems);*/
                                            ?>

                    <div class="clearfix"></div>
                                    <!-- Main Navigation / End -->


                    <!-- Header Widget -->
                                    <!--
                    <div class="header-widget">
                                            <a href="login-register.html" class="sign-in"><i class="fa fa-user"></i> Log In / Register</a>
                                            <a href="submit-property.html" class="button border">Submit Property</a>
                                    </div>
                    -->
                                    <!-- Header Widget / End -->
                            </div>
                            <!-- Right Side Content / End -->

                    </div>
            </div>
            <!-- Header / End -->

    </header>
    <div class="clearfix"></div>
    <!-- Header Container / End -->

