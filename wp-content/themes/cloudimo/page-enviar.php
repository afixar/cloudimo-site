<?php

/* 
 * Enviar Mensagem para Corretor
 */


if (isset ($_SESSION['mensagem_enviada'])) {
    
    exit('Uma mensagem já foi enviada.');
    
}


if (isset ($_POST['enviar'])) {
    
    
    $pattern_email = '/^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/';
    
        
    $to = '';
    
    //Captura email corretor
    foreach(cloudimo_xml_get_session('usuarios')->Usuario as $usuario) {
        if($usuario->ID == $_POST['corretor']) {
            $to = $usuario->Email;
            break;
        }
    }
    
    //$to = 'geovanisantos@afixar.com.br';
    
    if (! is_string($to)) {
        exit('Corretor inválido.');
    }
    
    
    if (! preg_match($pattern_email, $_POST['email'])) {
        exit('E-mail inválido.');
    }
    
    
    $subject = 'Cloudimo Site - Interresse de Imóvel';
    $msg = 'Cliente: '. $_POST['email'];
    $msg .= PHP_EOL;
    $msg .= 'Telefone: '.$_POST['telefone'];
    $msg .= PHP_EOL;
    $msg .= 'Mensagem: '.$_POST['mensagem'];
    
    
    if (strlen($msg) > 350) {
        exit('Mensagem muito longa.');
    }
    
    
    $headers = array('Content-Type: text/html; charset=UTF-8');
    $body = strip_tags($msg);
    
 
    wp_mail( $to, $subject, $body, $headers );

    
    $_SESSION['mensagem_enviada'] = true;
    exit('true');
    
}

exit('false');