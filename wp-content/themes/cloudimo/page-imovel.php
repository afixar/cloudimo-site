<?php

/*
 * Template Name: Detalhes Imóvel
 */


$id_imovel = (isset ($_GET['id']) && is_numeric($_GET['id'])) ? $_GET['id'] : false;


if (! $id_imovel) {
    get_page_404();
}


$imovel = '';        
cloudimo_verify_xml( cloudimo_get_xml('imoveis'). '&' . 'id='. $id_imovel , $imovel );


$imoveis_destaque = '';
cloudimo_buscar_imoveis($imoveis_destaque, [
    'act'           => 'pesquisar',
    'site_destaque' => 1
]);


//Caso não encontre imóvel
if ($imovel->Imoveis->Total == 0) {
    get_page_404();
} else {
    $imovel = $imovel->Imoveis->Imovel;
}


//Testar enviar mensagem.
//unset($_SESSION['mensagem_enviada']);

?>


<?php get_header(); ?>

<style>
    .property-slider-nav .item>img {
        width: 100%;
        height: 105px;
    }
</style>


<!-- Titlebar
================================================== -->
<div id="titlebar" class="property-titlebar margin-bottom-0">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				
                            <a href="<?=get_site_url()?>/procurar" class="back-to-listings"></a>
				<div class="property-title">
                                    <h5>Código: <?php echo $imovel->CodigoImovel; ?></h5>
                                    <h2><?php echo $imovel->TipoImovel; ?> <span class="property-badge"><?php echo $imovel->CategoriaImovel?></span></h2>
                                    <span>
                                        <div class="listing-address">
                                            <i class="fa fa-map-marker"></i>
                                            <?php echo $imovel->Bairro .' - '. $imovel->Cidade . ' - ' .$imovel->Uf; ?>
                                        </div>
                                    </span>
				</div>

				<div class="property-pricing">
                                    <h3>
                                        <?php if ($imovel->CategoriaImovel == 'Venda/Locação'): ?>
                                            <span>Venda:</span> R$ <?=$imovel->PrecoVenda?>
                                            <div class="margin-top-10"></div>
                                            <span>Locação:</span> R$ <?=$imovel->PrecoLocacao?>
                                        <?php endif; ?>

                                        <?php if ($imovel->CategoriaImovel == 'Locação'): ?>
                                            <span>Locação:</span> R$ <?=$imovel->PrecoLocacao?>
                                        <?php endif; ?>

                                        <?php if ($imovel->CategoriaImovel == 'Venda'): ?>
                                            <span>Venda:</span> R$ <?=$imovel->PrecoVenda?>
                                        <?php endif; ?>                                    
                                    </h3>
                                    <!-- <div class="sub-price">$770 / sq ft</div>-->
                                    
				</div>


			</div>
		</div>
	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
    <div class="row margin-bottom-50">
        <div class="col-md-12">
            
            <!-- Slider -->
            <div class="property-slider default">
                
                <?php foreach($imovel->Fotos->Foto as $foto): ?>
                    <a href="<?=cloudimo_get_image($foto->URLArquivo, false)?>" data-background-image="<?=cloudimo_get_image($foto->URLArquivo, false)?>" class="item mfp-gallery"></a>
                <?php endforeach; ?>
                    
            </div>

            <div class="property-slider-nav">
                <?php foreach($imovel->Fotos->Foto as $foto): ?>
                <div class="item"><img src="<?=cloudimo_get_image($foto->URLArquivo)?>" alt=""></div>
                <?php endforeach; ?>
            </div>
            
        </div>
    </div>
</div>


<div class="container">
	<div class="row">

		<!-- Property Description -->
		<div class="col-lg-8 col-md-7">
			<div class="property-description">

				<!-- Main Features --
				<ul class="property-main-features">
					<li>Area <span>1450 sq ft</span></li>
					<li>Rooms <span>4</span></li>
					<li>Bedrooms <span>2</span></li>
					<li>Bathrooms <span>1</span></li>
				</ul>-->


				<!-- Description -->
				<h3 class="desc-headline">Descrição</h3>
                                
                                <?php if ((string) $imovel->Descricao): ?>                                    
				<div class="show-more">
                                    <?=$imovel->Descricao?>
                                    <a href="#" class="show-more-button">Ler mais <i class="fa fa-angle-down"></i></a>
				</div>
                                <?php else: ?>
                                    Este imóvel não possui informações de descrição.
                                <?php endif; ?>

				<!-- Details -->
				<h3 class="desc-headline">Detalhes</h3>
				<ul class="property-features margin-top-0">                                    
                                    
                                    <?php $detalhe=false; ?>
                                    
                                    <?php if ((int) $imovel->QtdSalas) {  $detalhe=true;  echo "<li><i class='fa fa-home'></i>  {$imovel->QtdSalas} Sala</li>"; } ?>
                                    <?php if ((int) $imovel->QtdSuites) {  $detalhe=true; echo "<li><i class='fa fa-bed'></i>  {$imovel->QtdSuites} Suíte</li>"; } ?>
                                    <?php if ((int) $imovel->QtdDormitorios) { $detalhe=true; echo "<li><i class='fa fa-bed'></i>  {$imovel->QtdDormitorios} Quarto</li>"; } ?>
                                    <?php if ((int) $imovel->QtdBanheiros) { $detalhe=true; echo "<li><i class='fa fa-bath'></i>  {$imovel->QtdBanheiros} Banheiro</li>"; } ?>
                                    <?php if ((int) $imovel->QtdVagasGaragem) { $detalhe=true; echo "<li><i class='fa fa-car'></i>  {$imovel->QtdVagasGaragem} Garagem</li>"; } ?>
                                    <?php if ((int) $imovel->AreaUtil) { $detalhe=true; echo "<li><i class='fa fa-expand'></i>  {$imovel->AreaUtil} m² Área útil</li>"; } ?>
                                    <?php if ((int) $imovel->AreaContruida) { $detalhe=true; echo "<li><i class='fa fa-expand'></i>  {$imovel->AreaContruida} m² Área construída</li>"; } ?>
                                    <?php if ((int) $imovel->AreaTerreno) { $detalhe=true; echo "<li><i class='fa fa-expand'></i>  {$imovel->AreaTerreno} m² Área terreno</li>"; } ?>
                                     
                                    <?php if (!$detalhe) { echo 'Nenhum detalhe informado.'; }?>
                                    
				</ul>


				<!-- Features -->
                                <?php
                                
                                $observacoes = explode(',', $imovel->Observacao); 
                                $observacoes = array_filter($observacoes);
                                
                                if ($observacoes): ?>                                
				<h3 class="desc-headline">Características</h3>                               
				<ul class="property-features checkboxes margin-top-0">
                                    <?php foreach ($observacoes as $obs): ?>
					<li><?=$obs?></li>
                                    <?php endforeach; ?>
				</ul>
                                <?php endif; ?>


				<!-- Location -- >
				<h3 class="desc-headline no-border" id="location">Location</h3>
				<div id="propertyMap-container">
					<div id="propertyMap" data-latitude="40.7427837" data-longitude="-73.11445617675781"></div>
					<a href="#" id="streetView">Street View</a>
				</div> -->


				<!-- Similar Listings Container -- >
				<h3 class="desc-headline no-border margin-bottom-35 margin-top-60">Similar Properties</h3>

				<!-- Layout Switcher -- >

				<div class="layout-switcher hidden"><a href="#" class="list"><i class="fa fa-th-list"></i></a></div>
				<div class="listings-container list-layout">

					<!-- Listing Item -- >
					<div class="listing-item">

						<a href="#" class="listing-img-container">

							<div class="listing-badges">
								<span>For Rent</span>
							</div>

							<div class="listing-img-content">
								<span class="listing-price">$1700 <i>monthly</i></span>
								<span class="like-icon"></span>
							</div>

							<img src="php echo get_template_directory_uri();?>/images/listing-03.jpg" alt="">

						</a>
						
						<div class="listing-content">

							<div class="listing-title">
								<h4><a href="#">Meridian Villas</a></h4>
								<a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&hl=en&t=v&hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="listing-address popup-gmaps">
									<i class="fa fa-map-marker"></i>
									778 Country St. Panama City, FL
								</a>

								<a href="#" class="details button border">Details</a>
							</div>

							<ul class="listing-details">
								<li>1450 sq ft</li>
								<li>1 Bedroom</li>
								<li>2 Rooms</li>
								<li>2 Rooms</li>
							</ul>

							<div class="listing-footer">
								<a href="#"><i class="fa fa-user"></i> Chester Miller</a>
								<span><i class="fa fa-calendar-o"></i> 4 days ago</span>
							</div>

						</div>
						<!-- Listing Item / End -- >

					</div>
					<!-- Listing Item / End -- >

				</div>
				<!-- Similar Listings Container / End -->
                                
			</div>
                    
		</div>
		<!-- Property Description / End -->


		<!-- Sidebar -->
		<div class="col-lg-4 col-md-5">
			<div class="sidebar sticky right">

				<!-- Widget --
				<div class="widget margin-bottom-30">
					<button class="widget-button"><i class="sl sl-icon-printer"></i> Print</button>
					<button class="widget-button save" data-save-title="Save" data-saved-title="Saved"><span class="like-icon"></span></button>
				</div>
				!-- Widget / End -->


				<!-- Widget -->
				<div class="widget">

					<!-- Agent Widget -->
					<div class="agent-widget">
                                            
                                            <form id="form-enviar-mensagem" method="post">
                                            
                                                <div class="agent-title">

                                                <?php
                                                foreach(cloudimo_xml_get_session('usuarios')->Usuario as $usuario) :
                                                    if($usuario->ID == $imovel->IDUsuario): ?>

                                                    <div class="agent-photo"><img src="<?php echo get_template_directory_uri();?>/images/user_default.png" alt="" /></div>
                                                    <div class="agent-details">
                                                        <h4><a href="#"><?=get_nome_reduzido($usuario->Nome)?></a></h4>
                                                        <?php if (is_string($usuario->Telefone)): ?>
                                                        <span><i class="sl sl-icon-call-in"></i> <?=$usuario->Telefone?></span>
                                                        <?php endif; ?>
                                                    </div>

                                                    <input type="hidden" name="corretor" value="<?php echo $usuario->ID;?>">
                                                    
                                                <?php endif; endforeach; ?>       

                                                    <div class="clearfix"></div>
                                                </div>

                                                <p class="msg bg-success" id="notification-success" style="display:none">
                                                    Obrigado por sua mensagem.
                                                </p>
                                                
                                                
                                                <p class="msg bg-danger" id="notification-error" style="display:none">
                                                    Sua mensagem não pode ser enviada.
                                                </p>
                                                
                                                
                                                <input type="text" name="email" placeholder="E-mail *" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$">
                                                <input type="text" name="telefone" placeholder="Telefone">
                                                <textarea name="mensagem">Estou interessado no imóvel [cod <?=strtolower($imovel->CodigoImovel)?>] e gostaria saber mais detalhes.</textarea>
                                                
                                                <?php if (!isset ($_SESSION['mensagem_enviada'])) : ?>
                                                <button type="submit" class="button fullwidth margin-top-5" name="enviar">Enviar</button>
                                                <?php else: ?>
                                                <button class="button fullwidth margin-top-5" style="opacity:0.45" disabled="disabled">Mensagem enviada!</button>
                                                <?php endif; ?>
                                            </form>
                                                
					</div>
					<!-- Agent Widget / End -->

				</div>
				<!-- Widget / End -->


				<!-- Widget -->
				<div class="widget">
					<h3 class="margin-bottom-35">Imóveis em destaque</h3>

					<div class="listing-carousel outer">
                                            
                                            <?php foreach ($imoveis_destaque->Imoveis->Imovel as $key => $imovel): ?>

                                            <!-- Item -->
                                            <div class="item">
                                                    <div class="listing-item compact">


                                                        <a href="<?= get_page_link(8) ?>?id=<?= $imovel->ID ?>" class="listing-img-container">

                                                            <div class="listing-badges">
                                                                <?php $tarja = (string) $imovel->Tarja;
                                                                if ($tarja): ?>
                                                                    <span class="featured"><?= $tarja ?></span>
                                                                <?php endif; ?>
                                                                <span><?= $imovel->CategoriaImovel ?></span>
                                                            </div>

                                                            <div class="listing-img-content">                                                    

                                                                <span class="listing-compact-title">
                                                                    
                                                                    <?php echo $imovel->TipoImovel; ?>

                                                                    <?php if ($imovel->CategoriaImovel == 'Venda/Locação'): ?>
                                                                        <i>Venda R$ <?= $imovel->PrecoVenda ?></i>
                                                                        <i>Locação R$ <?= $imovel->PrecoLocacao ?></i>
                                                                    <?php endif; ?>

                                                                    <?php if ($imovel->CategoriaImovel == 'Locação'): ?>
                                                                        <i>R$ <?= $imovel->PrecoLocacao ?></i>
                                                                    <?php endif; ?>

                                                                    <?php if ($imovel->CategoriaImovel == 'Venda'): ?>
                                                                        <i>R$ <?= $imovel->PrecoVenda ?></i>
                                                                    <?php endif; ?>

                                                                </span>


                                                                <ul class="listing-hidden-content">
                                                                        <?php if ((int) $imovel->QtdSalas) echo "<li><i class='fa fa-home'></i>  {$imovel->QtdSalas} Sala</li>"; ?>
                                                                        <?php if ((int) $imovel->QtdSuites) echo "<li><i class='fa fa-bed'></i>  {$imovel->QtdSuites} Suíte</li>"; ?>
                                                                        <?php if ((int) $imovel->QtdDormitorios) echo "<li><i class='fa fa-bed'></i>  {$imovel->QtdDormitorios} Quarto</li>"; ?>
                                                                        <?php if ((int) $imovel->QtdBanheiros) echo "<li><i class='fa fa-bath'></i>  {$imovel->QtdBanheiros} Banheiro</li>"; ?>
                                                                        <?php if ((int) $imovel->QtdVagasGaragem) echo "<li><i class='fa fa-car'></i>  {$imovel->QtdVagasGaragem} Garagem</li>"; ?>
                                                                </ul>

                                                                

                                                            </div>

                                                            <img src="<?= cloudimo_get_image($imovel->Fotos->Foto->URLArquivo) ?>" alt="">
                                                            
                                                        </a>

                                                    </div>
                                            </div>
                                            <!-- Item / End -->

                                            <?php endforeach; ?>
                                                
					</div>

				</div>
				<!-- Widget / End -->

			</div>
		</div>
		<!-- Sidebar / End -->

	</div>
</div>


<!-- Footer
================================================== -->
<div class="margin-top-55"></div>



<!-- Begin Script JS -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript">

    jQuery(document).ready(function(){
       
        id_form = '#form-enviar-mensagem';
           
           
        $(id_form).validate({ 
            
            rules: {
                email: {
                    required: true,
                    email: true
                },                
            },
            messages: {
                email: {
                    required: "Por favor forneça um e-mail.",
                    email: "E-mail inválido."
                }
            },
            submitHandler: function(form) {
                
                
                $(id_form + ' #notification-success').hide();
                $(id_form + ' #notification-error').html('Sua mensagem não pode ser enviada.').hide();
                
                
                $(id_form+' button[name="enviar"]')
                    .attr('disabled', 'disabled')
                    .css('opacity', '0.45')
                    .html('Enviando...');
               
                
                $.ajax({
                    type: "POST",
                    url: '<?php echo get_page_link(get_id_by_slug('enviar'))?>',
                    data:  $(form).serialize(),
                    //dataType: 'post',
                    success: function(data) {
                       
                        if (!data) {
                            $(id_form + ' #notification-error').show();
                            return false;
                        }
                       
                        if (data !== 'true') {
                            $(id_form + ' #notification-error')
                                .html(data)
                                .show();
                        
                                
                            $(id_form+' button[name="enviar"]')
                                .removeAttr('disabled')
                                .removeAttr('style')
                                .html('Enviar');
                        
                            return false;
                        }
                       
                        $(id_form + ' #notification-success').show();

                        $(id_form+' button[name="enviar"]')
                             .html('Mensagem enviada!');                       
                        
                    },
                    error: function() {
                        $(id_form + ' #notification-error').show();
                        $(id_form+' button[name="enviar"]')
                            .removeAttr('disabled')
                            .removeAttr('style')
                            .html('Enviar');
                    }
                });
                
                
                return false;
            }

        });
       
       
    });
    
</script>
<!-- End Script JS -->



<!-- Maps --
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
<script type="text/javascript" src="scripts/infobox.min.js"></script>
<script type="text/javascript" src="scripts/markerclusterer.js"></script>
<script type="text/javascript" src="scripts/maps.js"></script> -->

<?php get_footer(); ?>
