<?php 

$imoveis = '';
cloudimo_buscar_imoveis($imoveis, [
    'act'           => 'pesquisar',
    'site_destaque' => 1
]);

?>

<!-- Content
================================================== -->
<div class="container">
    <div class="row">

        <div class="col-md-12">
            <h3 class="headline margin-bottom-25 margin-top-65">Em destaque</h3>
        </div>

        <!-- Carousel -->
        <div class="col-md-12">
            <div class="carousel">

                
                
                
                <?php foreach ($imoveis->Imoveis->Imovel as $key => $imovel): ?>

                <!-- Listing Item -->
                <div class="carousel-item">
                    
                    
                    <div class="listing-item">

                            <a href="<?=get_page_link(8)?>?id=<?=$imovel->ID?>" class="listing-img-container">

                                <div class="listing-badges">
                                    <?php $tarja = (string) $imovel->Tarja; if($tarja): ?>
                                    <span class="featured"><?=$tarja?></span>
                                    <?php endif; ?>
                                    <span><?=$imovel->CategoriaImovel?></span>
                                </div>

                                <div class="listing-img-content">                                                    

                                    <?php if ($imovel->CategoriaImovel == 'Venda/Locação'): ?>
                                    <span class="listing-price">Venda R$ <?=$imovel->PrecoVenda?></i></span>
                                    <span class="listing-price">Locação R$ <?=$imovel->PrecoLocacao?></i></span>
                                    <?php endif; ?>

                                    <?php if ($imovel->CategoriaImovel == 'Locação'): ?>
                                    <span class="listing-price">R$ <?=$imovel->PrecoLocacao?></i></span>
                                    <?php endif; ?>

                                    <?php if ($imovel->CategoriaImovel == 'Venda'): ?>
                                    <span class="listing-price">R$ <?=$imovel->PrecoVenda?></i></span>
                                    <?php endif; ?>

                                    <!-- <span class="like-icon tooltip"></span>-->

                                </div>

                                <img src="<?=cloudimo_get_image($imovel->Fotos->Foto->URLArquivo)?>" alt="">

                                <!--
                                <div class="listing-carousel">
                                    <div><img src="< ?php echo get_template_directory_uri(); ?>/images/listing-01.jpg" alt=""></div>
                                    <div><img src="< ?php echo get_template_directory_uri(); ?>/images/listing-01b.jpg" alt=""></div>
                                    <div><img src="< ?php echo get_template_directory_uri(); ?>/images/listing-01c.jpg" alt=""></div>
                                </div>
                                -->
                            </a>

                            <div class="listing-content">

                                <div class="listing-title">
                                        <h6>Código: <?php echo $imovel->CodigoImovel; ?></h6>
                                        <h4><a href="<?=get_page_link(8)?>?id=<?=$imovel->ID?>"><?php echo $imovel->TipoImovel; ?></a></h4>
                                        <a href="#" class="listing-address popup-gmaps">
                                                <i class="fa fa-map-marker"></i>
                                                <?php echo $imovel->Bairro . ' - ' . $imovel->Cidade . ' - ' . $imovel->Uf; ?>
                                        </a>

                                        <a href="<?=get_page_link(8)?>?id=<?=$imovel->ID?>" class="details button border">Mais detalhes</a>
                                </div>

                                <ul class="listing-details">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <?php if ((int) $imovel->QtdSalas) echo "<li><i class='fa fa-home'></i>  {$imovel->QtdSalas} Sala</li>"; ?>
                                            <?php if ((int) $imovel->QtdSuites) echo "<li><i class='fa fa-bed'></i>  {$imovel->QtdSuites} Suíte</li>"; ?>
                                            <?php if ((int) $imovel->QtdDormitorios) echo "<li><i class='fa fa-bed'></i>  {$imovel->QtdDormitorios} Quarto</li>"; ?>
                                        </div>
                                        <div class="col-md-6">
                                            <?php if ((int) $imovel->QtdBanheiros) echo "<li><i class='fa fa-bath'></i>  {$imovel->QtdBanheiros} Banheiro</li>"; ?>
                                            <?php if ((int) $imovel->QtdVagasGaragem) echo "<li><i class='fa fa-car'></i>  {$imovel->QtdVagasGaragem} Garagem</li>"; ?>
                                        </div>
                                    </div>
                                </ul>

                                <div class="listing-footer" style="display: none;">
                                    <i class="fa fa-user"></i>
                                        <?php
                                        foreach(cloudimo_xml_get_session('usuarios')->Usuario as $usuario){
                                            if($usuario->ID == $imovel->IDUsuario) {
                                                echo $usuario->Nome; break;
                                            }
                                        }                                                       
                                        ?>
                                        <!--<span><i class="fa fa-calendar-o"></i> <?=$imovel->DataResumo?></span>-->
                                </div>
                            </div>

                    </div>
                                             
                
                </div>
                <!-- Listing Item / End -->
                <?php endforeach; ?>
                
                
                
                
                
                <!-- Listing Item -- >
                <div class="carousel-item">

                    <div class="listing-item">

                        <a href="single-property-page-1.html" class="listing-img-container">

                            <div class="listing-badges">
                                <span class="featured">Featured</span>
                                <span>For Sale</span>
                            </div>

                            <div class="listing-img-content">
                                <span class="listing-price">$275,000 <i>$520 / sq ft</i></span>
                                <span class="like-icon tooltip"></span>
                            </div>

                            <div class="listing-carousel">
                                <div><img src="php echo get_template_directory_uri(); ?>/images/listing-01.jpg" alt=""></div>
                                <div><img src="php echo get_template_directory_uri(); ?>/images/listing-01b.jpg" alt=""></div>
                                <div><img src="php echo get_template_directory_uri(); ?>/images/listing-01c.jpg" alt=""></div>
                            </div>

                        </a>

                        <div class="listing-content">

                            <div class="listing-title">
                                <h4><a href="single-property-page-1.html">Eagle Apartments</a></h4>
                                <a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&hl=en&t=v&hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="listing-address popup-gmaps">
                                    <i class="fa fa-map-marker"></i>
                                    9364 School St. Lynchburg, NY
                                </a>
                            </div>

                            <ul class="listing-features">
                                <li>Area <span>530 sq ft</span></li>
                                <li>Bedrooms <span>2</span></li>
                                <li>Bathrooms <span>1</span></li>
                            </ul>

                            <div class="listing-footer">
                                <a href="#"><i class="fa fa-user"></i> David Strozier</a>
                                <span><i class="fa fa-calendar-o"></i> 1 day ago</span>
                            </div>

                        </div>

                    </div>
                </div>
                <!-- Listing Item / End -- >


                <!-- Listing Item -- >
                <div class="carousel-item">
                    <div class="listing-item">

                        <a href="single-property-page-2.html" class="listing-img-container">

                            <div class="listing-badges">
                                <span>For Rent</span>
                            </div>

                            <div class="listing-img-content">
                                <span class="listing-price">$900 <i>monthly</i></span>
                                <span class="like-icon"></span>
                            </div>

                            <img src="php echo get_template_directory_uri(); ?>/images/listing-02.jpg" alt="">

                        </a>

                        <div class="listing-content">

                            <div class="listing-title">
                                <h4><a href="single-property-page-2.html">Serene Uptown</a></h4>
                                <a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&hl=en&t=v&hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="listing-address popup-gmaps">
                                    <i class="fa fa-map-marker"></i>
                                    6 Bishop Ave. Perkasie, PA
                                </a>
                            </div>

                            <ul class="listing-features">
                                <li>Area <span>440 sq ft</span></li>
                                <li>Bedrooms <span>2</span></li>
                                <li>Bathrooms <span>1</span></li>
                            </ul>

                            <div class="listing-footer">
                                <a href="#"><i class="fa fa-user"></i> Harriette Clark</a>
                                <span><i class="fa fa-calendar-o"></i> 2 days ago</span>
                            </div>

                        </div>

                    </div>
                </div>
                <!-- Listing Item / End -- >


                <!-- Listing Item -- >
                <div class="carousel-item">
                    <div class="listing-item">


                        <a href="single-property-page-1.html" class="listing-img-container">

                            <div class="listing-badges">
                                <span class="featured">Featured</span>
                                <span>For Rent</span>
                            </div>

                            <div class="listing-img-content">
                                <span class="listing-price">$1700 <i>monthly</i></span>
                                <span class="like-icon"></span>
                            </div>

                            <img src="php echo get_template_directory_uri(); ?>/images/listing-03.jpg" alt="">

                        </a>

                        <div class="listing-content">

                            <div class="listing-title">
                                <h4><a href="single-property-page-1.html">Meridian Villas</a></h4>
                                <a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&hl=en&t=v&hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="listing-address popup-gmaps">
                                    <i class="fa fa-map-marker"></i>
                                    778 Country St. Panama City, FL
                                </a>
                            </div>

                            <ul class="listing-features">
                                <li>Area <span>1450 sq ft</span></li>
                                <li>Bedrooms <span>2</span></li>
                                <li>Bathrooms <span>3</span></li>
                            </ul>

                            <div class="listing-footer">
                                <a href="#"><i class="fa fa-user"></i> Chester Miller</a>
                                <span><i class="fa fa-calendar-o"></i> 4 days ago</span>
                            </div>

                        </div>
                        <!-- Listing Item / End -- >

                    </div>
                </div>
                <!-- Listing Item / End -- >


                <!-- Listing Item -- >
                <div class="carousel-item">
                    <div class="listing-item">


                        <a href="single-property-page-3.html" class="listing-img-container">

                            <div class="listing-badges">
                                <span>For Sale</span>
                            </div>

                            <div class="listing-img-content">
                                <span class="listing-price">$420,000 <i>$770 / sq ft</i></span>
                                <span class="like-icon"></span>
                            </div>

                            <div class="listing-carousel">
                                <div><img src="php echo get_template_directory_uri(); ?>/images/listing-04.jpg" alt=""></div>
                                <div><img src="php echo get_template_directory_uri(); ?>/images/listing-04b.jpg" alt=""></div>
                            </div>

                        </a>

                        <div class="listing-content">

                            <div class="listing-title">
                                <h4><a href="single-property-page-3.html">Selway Apartments</a></h4>
                                <a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&hl=en&t=v&hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="listing-address popup-gmaps">
                                    <i class="fa fa-map-marker"></i>
                                    33 William St. Northbrook, IL
                                </a>
                            </div>

                            <ul class="listing-features">
                                <li>Area <span>540 sq ft</span></li>
                                <li>Bedrooms <span>2</span></li>
                                <li>Bathrooms <span>2</span></li>
                            </ul>

                            <div class="listing-footer">
                                <a href="#"><i class="fa fa-user"></i> Kristen Berry</a>
                                <span><i class="fa fa-calendar-o"></i> 3 days ago</span>
                            </div>

                        </div>
                        <!-- Listing Item / End -- >

                    </div>
                </div>
                <!-- Listing Item / End -- >


                <!-- Listing Item -- >
                <div class="carousel-item">
                    <div class="listing-item">


                        <a href="single-property-page-1.html" class="listing-img-container">
                            <div class="listing-badges">
                                <span>For Sale</span>
                            </div>

                            <div class="listing-img-content">
                                <span class="listing-price">$535,000 <i>$640 / sq ft</i></span>
                                <span class="like-icon"></span>
                            </div>

                            <img src="php echo get_template_directory_uri(); ?>/images/listing-05.jpg" alt="">
                        </a>

                        <div class="listing-content">

                            <div class="listing-title">
                                <h4><a href="single-property-page-1.html">Oak Tree Villas</a></h4>
                                <a href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&hl=en&t=v&hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom" class="listing-address popup-gmaps">
                                    <i class="fa fa-map-marker"></i>
                                    71 Lower River Dr. Bronx, NY
                                </a>
                            </div>

                            <ul class="listing-features">
                                <li>Area <span>350 sq ft</span></li>
                                <li>Bedrooms <span>2</span></li>
                                <li>Bathrooms <span>1</span></li>
                            </ul>

                            <div class="listing-footer">
                                <a href="#"><i class="fa fa-user"></i> Mabel Gagnon</a>
                                <span><i class="fa fa-calendar-o"></i> 4 days ago</span>
                            </div>

                        </div>
                        <!-- Listing Item / End -- >

                    </div>
                </div>
                <!-- Listing Item / End -->



            </div>
        </div>
        <!-- Carousel / End -->

    </div>
</div>

