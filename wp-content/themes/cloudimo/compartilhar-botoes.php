<!--- FACEBOOK -- >
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-share-button" data-href="https://afixar.com.br" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fafixar.com.br%2F&amp;src=sdkpreparse">Share</a></div>
< !-- FACEBOOK /-- >
< !-- TWITTER -- >
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<a href="https://twitter.com/share" class="twitter-share-button" data-lang="pt">Tweetar</a>
< !-- TWITTER /-- >
< !-- GOOGLE PLUS -- >
<script src="https://apis.google.com/js/platform.js" async defer>{lang: 'pt-BR'}</script>
<div class="g-plus" data-action="share" data-annotation="none"></div>
< !-- GOOGLE PLUS -->


<ul class="share-buttons margin-top-40 margin-bottom-0">
    <li>
        <a class="fb-share" href="javascript:;" data-href="http://www.facebook.com/share.php?u=<?php the_permalink();?>&t=<?php the_title();?>" target="_blank"><i class="fa fa-facebook"></i> Compartilhar</a>
    </li>
    <li>
        <a class="twitter-share" href="javascript:;" data-href="http://twitter.com/share?text=<?php the_title();?>&url=<?php the_permalink();?>" target="_blank"><i class="fa fa-twitter"></i> Tweetar</a>
    </li>
    <li>
        <a class="gplus-share" href="javascript:;" data-href="https://plus.google.com/share?url=<?php  the_permalink();?>" target="_blank"><i class="fa fa-google-plus"></i> Compartilhar</a>
    </li>
</ul>


<script type="text/javascript">
    $(document).ready(function(){
        
        var shareWindow = function(url, title){
            var x = screen.width/2 - 500/2;
            var y = screen.height/2 - 300/2;
            window.open(url, title,'height=300,width=500,left='+x+',top='+y);
        }
        
        $('a.fb-share').click(function(){
            shareWindow($(this).attr('data-href'), "Facebook");
        });
        $('a.twitter-share').click(function(){
            shareWindow($(this).attr('data-href'), "Twitter");
        });
        $('a.gplus-share').click(function(){
            shareWindow($(this).attr('data-href'), "Google Plus");
        });
        
    });
</script>



