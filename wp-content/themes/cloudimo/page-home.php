<?php
/* Template Name: Página Inicial */
get_header();
$id_home = get_id_by_slug('home');
$imagem_parallax = get_field('home_imagem_parallax', $id_home);

?>


<!-- Banner
================================================== -->
<div class="parallax" data-background="<?php echo $imagem_parallax['sizes']['large']?>" data-color="#36383e" data-color-opacity="0.5" data-img-width="2500" data-img-height="1600">

	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<div class="search-container">

					<!-- Form -->
					<h2 class="margin-bottom-15"><?php echo get_field('home_chamada', $id_home);?></h2>
                    <div class="announce"><?php echo get_field('home_retranca', $id_home);?></div>

                                        <form method="post" action="procurar">
					<!-- Row With Forms -->
					<div class="row with-forms">


						<!-- Property Type -->
						<div class="col-md-3">
                                                    <select name="tipo_imovel" data-placeholder="Any Type" class="chosen-select-no-single" >
                                                        <option value="">Tipo de imóvel</option>
                                                        <?php foreach(cloudimo_xml_get_session('tipo-imoveis')->Tipo as $tipo): ?>
                                                        <option value="<?=$tipo->ID?>"><?=$tipo->Nome?></option>
                                                        <?php endforeach;?>
                                                    </select>
						</div>


						<!-- Status -->
						<div class="col-md-3">
                                                    <select name="categoria" data-placeholder="Any Status" class="chosen-select-no-single" >
                                                        <option value="locacao">Para Locar</option>
                                                        <option value="venda">Para Comprar</option>
                                                        <option value="todos">Todos</option>
                                                    </select>
						</div>

						<!-- Main Search Input -->
						<div class="col-md-6">
                                                    <div class="main-search-input">
                                                        <input name="procurar" type="text" placeholder="Digite um bairro, cidade ou código do imóvel."/>
                                                        <button type="submit" name="act" value="pesquisar" class="button"><i class="fa fa-search"></i></button>
                                                    </div>
						</div>

					</div>
                                        </form>
					<!-- Row With Forms / End -->

					<!-- Browse Jobs -->
					<div class="adv-search-btn">
						Deseja mais opções <a href="procurar">Faça uma busca avançada</a>
					</div>

					<!-- Announce --
					<div class="announce">
						Temos 1000 imóveis esperando por você!
					</div>-->

				</div>

			</div>
		</div>
	</div>

</div>



<!--- Destaque -->
<?php include( get_query_template('destaque') ); ?>
<!-- End destaque -->


<!-- Bloco o que fazemos-->
<?php include( get_query_template('bloco-servicos') ); ?>
<!-- End Bloco o que fazemos-->


<!-- Bloco o que fazemos-->
<?php include( get_query_template('bloco-informativo') ); ?>
<!-- End Bloco informativo-->


<!-- Flip banner -->
<a href="<?php echo get_permalink( get_id_by_slug('procurar') ); ?>" data-background="<?php echo $imagem_parallax['sizes']['large']?>" class="flip-banner parallax" data-color-opacity="0.8" data-img-width="2500" data-img-height="1600">
    <div class="flip-banner-content">
        <h2 class="flip-visible"><?php echo get_field('home_chamada_rodape', $id_home); ?></h2>
        <h2 class="flip-hidden">Procurar Imóveis <i class="sl sl-icon-arrow-right"></i></h2>
    </div>
</a>
<!-- Flip banner / End -->


<?php get_footer();?>
