<?php
/* Template Name: Página Sobre */
get_header();
?>


<!-- Titlebar
================================================== -->
<div id="titlebar">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <h2>Conheça-nos</h2>

            </div>
        </div>
    </div>
</div>



<!-- Content
================================================== -->
<div class="container">

	<!-- Inline Elements
	================================================== -->
        <?php while ( have_posts() ) : the_post(); ?>
	<div class="row">
            <div class="col-md-12 margin-bottom-90">
            <!-- Headline -->
            <h4 class="headline with-border"><?php the_title(); ?></h4>
            <?php the_content(); ?>
            </div>
	</div>
        <?php endwhile; ?>

</div>



<?php get_footer();?>
