<?php

/**
 * Arquivo responsável por integrar todos arquivos
 * para funcionalidades das requisições
 * ao Sistema Cloudimo.
 */


// Includes e requires
require_once('config.php');
require_once('get_xml.php');
require_once('page.php');
require_once('verify_xml.php');
require_once('session_xml.php');
require_once('buscar_imoveis.php');
require_once('pagination.php');



/**
 * @param string $path 
 * @param bool $thumb
 */
function cloudimo_get_image($path = null, $thumb = true)
{    
    
    if ($path) {
                
        if ($thumb) {
            $pattern = '@(upload\/publico\/[\d]+)\/imobiliaria\/(.*)@';
            $path = preg_replace($pattern, '$1/thumb/imobiliaria/$2', $path);
        }        
        
        return $path;
        
    }
    
    return get_template_directory_uri() . '/images/no-image.png';
    
}