<?php
/**
 * Layout do formulário
 */
function cloudimo_config_page() { ?>

    <div class="wrap">

    <h1>Configurações de XML</h1>

    <form method="post" action="options.php">

        <?php settings_fields( 'cloudimo-plugin-settings-group' ); ?>
        <?php do_settings_sections( 'cloudimo-plugin-settings-group' ); ?>

        <table class="form-table">

            <tr valign="top">
            <th scope="row">XML url principal</th>
            <td><input type="text" style="width:50%" name="cloudimo_xml_url" value="<?php echo esc_attr( get_option('cloudimo_xml_url') ); ?>" /></td>
            </tr>

            <!--
            <tr valign="top">
            <th scope="row">XML Imóveis</th>
            <td><input type="text" style="width:50%" name="cloudimo_xml_imoveis" value="< ?php echo esc_attr( get_option('cloudimo_xml_imoveis') ); ?>" /></td>
            </tr>
            
            <tr valign="top">
            <th scope="row">XML Tipo de Imóveis</th>
            <td><input type="text" style="width:50%" name="cloudimo_xml_tipo_imoveis" value="< ?php echo esc_attr( get_option('cloudimo_xml_tipo_imoveis') ); ?>"/></td>
            </tr>


            <tr valign="top">
            <th scope="row">XML Corretores</th>
            <td><input type="text" style="width:50%" name="cloudimo_xml_usuarios" value="< ?php echo esc_attr( get_option('cloudimo_xml_usuarios') ); ?>"/></td>
            </tr>-->

            <tr valign="top">
            <th scope="row">Chave de acesso</th>
            <td><input type="text" style="width:50%" name="cloudimo_key" value="<?php echo esc_attr( get_option('cloudimo_key') ); ?>" /></td>
            </tr>

        </table>

        <?php submit_button(); ?>

    </form>
    </div>

<?php } //End cloudimo_config_page ?>