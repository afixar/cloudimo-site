<?php

/**
 * Imprimi paginação
 * 
 * @param int $atual_pag
 * @param int $total_pag
 * @param string $link (?pag=) Nome de $_GET pagina
 */
function cloudimo_html_pagination($atual_pag, $total_pag, $link = '?pag=')
{
    
    $i = 1;

    $html = '<nav class="pagination">';
    $html .= '<ul>';

    while ($i <= $total_pag) {

        $href = 'href="'.$link.''.$i.'"';

        $html .= '<li><a '.$href;

        //Pagina atual
        if ($i == $atual_pag) {
            $html .= 'class="current-page"';
        }


        $html .= '>'. $i .'</a></li>';

        
        //Pagina anterior
        if ($i == $atual_pag - 1) {
            $html_anterior = '<li><a '.$href.' class="prev">Anterior</a></li>';
        }

        //Pagina seguinte
        if ($i == $atual_pag + 1) {
            $html_seguinte = '<li><a '.$href.' class="next">Próximo</a></li>';
        }

        $i++;
    }

    $html .= '</ul>';
    $html .= '</nav>';

    $html .= '<nav class="pagination-next-prev">';
    $html .= '<ul>';   
    $html .=  $html_anterior;
    $html .=  $html_seguinte;
    $html .= '</ul>';
    $html .= '</nav>';

    return $html;

}
