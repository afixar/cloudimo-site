<?php
/** 
 * Recupera caminho do xml de consulta ao cloudimo
 * @param string $nome imoveis, tipo_imoveis e usuarios
 */
function cloudimo_get_xml($nome)
{
    
    $key = get_option('cloudimo_key');
    $url = get_option('cloudimo_xml_url');    
    
    if (! $url )
        throw new \Exception('Não foi encontrado caminho de Cloudimo XML.');

    if (! $key )
        throw new \Exception('Não foi encontrado caminho de Cloudimo KEY.');
    
    $key = '&key=' . $key;
    
    switch ($nome) {
        
        case 'imoveis':
            return  $url . '/imoveis.xml' . $key;
            break;
        
        case 'tipo-imoveis':
            return  $url . '/tipo-imoveis.xml' . $key;
            break;
        
        case 'usuarios':
            return  $url . '/usuarios.xml' . $key;
            break;
        
        default:
            return  NULL;
            break;
        
    }    
    
}

