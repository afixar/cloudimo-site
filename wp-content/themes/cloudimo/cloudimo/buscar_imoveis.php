<?php

/**
 * Exemplo de post_request:
 * 
 *  $post_request = [
 *      'act'       =>'pesquisar',
 *      'categoria' =>'locacao',
 *  ];
 *  include_once('page-procurar.php');
 * 
 * 
 * @param mixed $result (Variável a receder xml)
 * @param array $post_request (null) Post personalizado
 * @param array $add_post (null) Dadicionar post
 */
function cloudimo_buscar_imoveis(&$result = null, $post_request = null, $add_post = null)
{
    
    
    if ($post_request && isset ($post_request['act'])) {
        $post = $post_request;
    }
    

    if ($post || $add_post) {

        //echo '<pre>post';print_r($post); exit;
        
        $search = [
            'id_tipo_imovel'    => isset ($post['tipo_imovel'])      ? $post['tipo_imovel']      : null,
            'categoria'         => isset ($post['categoria'])        ? $post['categoria']        : null,
            'procurar'          => isset ($post['procurar'])         ? $post['procurar']         : null,
            'area_util_de'      => isset ($post['area_util_de'])     ? $post['area_util_de']     : null,
            'area_util_ate'     => isset ($post['area_util_ate'])    ? $post['area_util_ate']    : null,
            'valor_de'          => isset ($post['valor_de'])         ? $post['valor_de']         : null,
            'valor_ate'         => isset ($post['valor_ate'])        ? $post['valor_ate']        : null,
            'garagem'           => isset ($post['garagem'])          ? $post['garagem']          : null,
            'salas'             => isset ($post['salas'])            ? $post['salas']            : null,
            'quartos'           => isset ($post['quartos'])          ? $post['quartos']          : null,
            'banheiros'         => isset ($post['banheiros'])        ? $post['banheiros']        : null,
            'site_destaque'     => isset ($post['site_destaque'])    ? $post['site_destaque']    : null,
        ];
  
        
        if ($add_post) {
            $search = array_merge($search, $add_post);
        }
        
        
        $search = http_build_query($search);
        //exit('URL Search: '.cloudimo_get_xml('imoveis'). '&' . $search);
        
        cloudimo_verify_xml( cloudimo_get_xml('imoveis'). '&' . $search , $result );
        
    } else {
        //echo 'aqui'; exit;
        cloudimo_verify_xml( cloudimo_get_xml('imoveis') , $result );
    }
    
}

