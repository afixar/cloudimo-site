<?php
/**
 * Configuração geral
 */

// Adicionar função ao para administração Wordpress
add_action('admin_menu', 'cloudimo_menu_config');


/**
 * Esta função cria menu em administração Wordpress
 */
function cloudimo_menu_config() {

    //Adiciona menu para administração Wordpress
    add_menu_page('Cloudimo', 'Cloudimo', 'administrator', __FILE__, 'cloudimo_config_page' , get_template_directory_uri().'/cloudimo/images/ico.png' );

    //Registra função de registra campos de formulário
    add_action( 'admin_init', 'register_cloudimo_plugin_settings' );
        
}


/**
 * Esta função registra campos de formulário
 */
function register_cloudimo_plugin_settings() {
    
    register_setting( 'cloudimo-plugin-settings-group', 'cloudimo_xml_url' );
    register_setting( 'cloudimo-plugin-settings-group', 'cloudimo_key' );
    
}