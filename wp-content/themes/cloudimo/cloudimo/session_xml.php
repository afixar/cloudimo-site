<?php


if (get_option('cloudimo_xml_url')) {    
    criarCloudimoSessionXML();    
}


/**
 * Captura session guardadas para integração XML
 * @param string $key
 * @return array
 */
function cloudimo_xml_get_session($key=null)
{    
    if (isset($_SESSION['_cloudimo_xml'][$key]))
        return $_SESSION['_cloudimo_xml'][$key];
    
}



/**
 * Cria sessiona para guarda xml em session
 */
function criarCloudimoSessionXML() {
    
    if (isset ($_SESSION['_cloudimo_xml']) && $_SESSION['_cloudimo_xml']) {
        
        $s_time = new DateTime();    
        $s_time->modify('-2 hour');
        
        if ($s_time->getTimestamp() >= $_SESSION['_cloudimo_xml']['time']) {
            inserirCloudimoSessionXML();
        }
        
    } else {
       inserirCloudimoSessionXML();
    }
    
}


/**
 * Inserir e atualizar session de xml
 */
function inserirCloudimoSessionXML()
{
    
    /*
    $xml = XMLReader::open(cloudimo_get_xml('tipo-imoveis'));
    $xml->setParserProperty(XMLReader::VALIDATE, true);    
    $xml->isValid()
    */
    
    
    //Capturar xml de tipo de imóveis
    $tipo_imoveis = '';
    $usuarios = '';
   
    
    cloudimo_verify_xml(cloudimo_get_xml('tipo-imoveis'), $tipo_imoveis);
    cloudimo_verify_xml(cloudimo_get_xml('usuarios'), $usuarios);
    
    
    
    //Tipo imoveis para session
    $tipo_imoveis_arr = json_encode($tipo_imoveis->Tipos->children());
    $tipo_imoveis_arr = json_decode($tipo_imoveis_arr);

    
    //Usuarios para session
    $usuarios_arr = json_encode($usuarios->Usuarios->children());
    $usuarios_arr = json_decode($usuarios_arr);
   
    
    $_SESSION['_cloudimo_xml'] = [
        'time'          => time(),
        'tipo-imoveis'  => $tipo_imoveis_arr,
        'usuarios'    => $usuarios_arr
    ];
    
}

