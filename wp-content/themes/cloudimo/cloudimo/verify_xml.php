<?php

/**
 * Valida e retorna XML válido. Caso contrário
 * exibi página de erro XML.
 * 
 * @param string $url
 * @param string $xml_element (Reference para variável a receder SimpleXMLElement)
 * @return SimpleXMLElement se for válido
 */
function cloudimo_verify_xml($url = null, &$xml_element = null)
{
    
    $fopen = fopen($url, "r");
    $xml_element = stream_get_contents($fopen);
    fclose($fopen);
    
    if (! strstr($xml_element, 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"', TRUE)) {
        //status_header( 404 );
        nocache_headers();
        include( get_query_template('error-xml') );
        die();
    }
    
    $xml_element = new \SimpleXMLElement($xml_element);
    
    //echo '<pre>'; print_r($xml_element); exit;
    //new SimpleXMLElement(url, null, true);
    
}