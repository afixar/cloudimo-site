<?php

/**********************************************************************************************************************
 * Theme Support
 *********************************************************************************************************************/
/*error_reporting(E_ERROR | E_PARSE);*/

session_cache_limiter("public");
session_cache_expire(0);
session_start();


/**********************************************************************************************************************
 * Sessao
 *********************************************************************************************************************/
 
// Verifica se não existe nenhuma função com o nome _session_start
if ( ! function_exists( '_session_start' ) ) {
    // Cria a função
    function _session_start() {
        // Inicia uma sessão PHP
        if ( ! session_id() ) session_start();
    }
    // Executa a ação
    add_action( 'init', '_session_start' );

}


/**
 * Begin exibir imagem destaque
 */
add_theme_support( 'post-thumbnails' );
/*
 * End exibir imagem destaque
 */



/**********************************************************************************************************************
 * Cloudimo 
 *********************************************************************************************************************/

require_once ('cloudimo/webservice.php');


function get_nome_reduzido($nome) {
	
	$exp = explode(' ', $nome);
	
	if (count($exp) > 1){
		return current($exp) .' '. end($exp);
	}
	
}


/*add_theme_support( 'post-thumbnails' );*/


register_nav_menus( array(
    'primary' => __( 'Menu Principal'),
    //'footer' => __( 'Menu Rodapé'),
) );


//modo de compatibilidade time
setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');

// Verifica se não existe nenhuma função com o nome _session_start
if ( ! function_exists( '_session_newsletter' ) ) {
    // Cria a função
    function _session_newsletter() {
        // Inicia uma sessão PHP
        if ( ! session_id() ) session_start();
    }

    add_action( 'init', '_session_newsletter' );
}


/**********************************************************************************************************************
 * Functions
 *********************************************************************************************************************/
/**
 * Recursively sort an array of taxonomy terms hierarchically. Child categories will be
 * placed under a 'children' member of their parent term.
 * @param Array   $cats     taxonomy term objects to sort
 * @param Array   $into     result array to put them in
 * @param integer $parentId the current parent ID to put them in
 * http://wordpress.stackexchange.com/questions/14652/how-to-show-a-hierarchical-terms-list
 * $categories = get_terms('my_taxonomy_name', array('hide_empty' => false));
 *   $categoryHierarchy = array();
 *   sort_terms_hierarchicaly($categories, $categoryHierarchy);
 *   
 *   var_dump($categoryHierarchy);
 */
function sort_terms_hierarchicaly(Array &$cats, Array &$into, $parentId = 0)
{
    foreach ($cats as $i => $cat) {
        if ($cat->parent == $parentId) {
            $into[$cat->term_id] = $cat;
            unset($cats[$i]);
        }
    }

    foreach ($into as $topCat) {
        $topCat->children = array();
        sort_terms_hierarchicaly($cats, $topCat->children, $topCat->term_id);
    }
}

/**
 * @getPost
 * @example getPosts(array('post','agenda','receitas'));
 * @params $post_type = array();
 */
function getPosts($post_type,$limit='100') {

    $result = array();

    $the_query = new WP_Query(array('post_type' => $post_type,'orderby'=>'date', 'posts_per_page' => $limit));

    if($the_query->have_posts()):

        while ($the_query->have_posts()) : $the_query->the_post();

             
            
            $id              = get_the_ID();
            $url             = get_permalink();
            $thumb_id        = get_post_thumbnail_id();
            $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', false);
            if($thumb_url_array[0]) {  $thumb = $thumb_url_array[0]; } else { $thumb  = ''; }

            if(get_field('horario'))
            {
                   $date = get_the_date('d/m').' | '.get_field('horario');
            }
            else
            {
                   $date = get_the_date('d/m/y');
            }




            $result[$id]     = array('type'=>get_post_type(),'title'=>get_the_title(),'date'=>$date,'thumb'=>$thumb,'url'=>$url,'desc'=>get_field('descricao'));

        endwhile;

    endif;

    return $result;

}

/**
 * Paginação
 */
function fdPageNavi($wp_query,$format='?page=%#%'){


    $big = 9999999999;
    //echo "<div class='fd-paginacao'>";
    return paginate_links( array(
        /*'base'      => '%_%',*/
        'base'      => @add_query_arg('page','%#%'),
        'format'    => $format,
        'current'   => max( 1, get_query_var('page') ),
        'total'     => $wp_query->max_num_pages,
        'type'      => 'list',
        'add_args'  => true,
        'prev_next' => true,
        'prev_text' => 'Anterior',
        'next_text' => 'Próximo',
        'mid_size'  => 2,
        'end_size'  => 1
    ) );
    //echo "</div>";
}

/*
 * Remover Todas as Notiificações de Updates
 */

function remove_core_updates(){
global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
}
add_filter('pre_site_transient_update_core','remove_core_updates');
add_filter('pre_site_transient_update_plugins','remove_core_updates');
add_filter('pre_site_transient_update_themes','remove_core_updates');

/*
 * Resumir texto
 */
function resumir($texto, $limite, $quebra = false) 
{
	$tamanho = strlen($texto);

	// Verifica se o tamanho do texto é menor ou igual ao limite
	if ($tamanho <= $limite) {
		$novo_texto = $texto;
	// Se o tamanho do texto for maior que o limite
	} else {
		// Verifica a opção de quebrar o texto
		if ($quebra == true) {
			$novo_texto = trim(substr($texto, 0, $limite)).' ...';
			// Se não, corta $texto na última palavra antes do limite
		} else {
			// Localiza o útlimo espaço antes de $limite
			$ultimo_espaco = strrpos(substr($texto, 0, $limite), ' ');
			// Corta o $texto até a posição localizada
			$novo_texto = trim(substr($texto, 0, $ultimo_espaco)).' ...';
		}
	}

	// Retorna o valor formatado
	return $novo_texto;
} 
 
 /*
 * Menu class active
 */
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item)
{
    if (in_array('menu-auto-active', $classes)) {
        $classes[] = 'active ';
    }
    
    if (in_array('current-menu-item', $classes)) {
        $classes[] = ' current ';
    }
    
    return $classes;
}

/**
 * Filter the except length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function new_excerpt_length($length) {
    return 10;
}
add_filter('excerpt_length', 'new_excerpt_length');

/* crop img template thumbs */

add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
   add_image_size( 'informativos', 220, 180, true ); // (cropped)
   add_image_size( 'home_logo', 191, 67, true ); // (cropped)
   add_image_size( 'favicon', 32, 32, true ); // (cropped)
   add_image_size( 'favicon114', 114, 114, true ); // (cropped)
   add_image_size( 'favicon72', 72, 72, true ); // (cropped)
   add_image_size( 'favicon57', 57, 57, true ); // (cropped)
}


if (function_exists('acf_add_options_page')) {
    acf_add_options_page();
}

// get_id_by_slug('any-page-slug');
function get_id_by_slug($page_slug) {
	$page = get_page_by_path($page_slug);
	if ($page) {
		return $page->ID;
	} else {
		return null;
	}
}

/**
 * Exibir página 404
 */
function get_page_404()
{    
    status_header( 404 );
    nocache_headers();
    include( get_query_template('404') );
    die();
}


?>