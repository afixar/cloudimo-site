<?php
$args = array(
    'post_type'         => 'informativos',
    'orderby'           => 'meta_value',
    'order'             => 'DESC',
    'posts_per_page'    => 3,
);

$informativos = new WP_Query($args);



if($informativos->have_posts())
{
?>



<!-- Container -->
<div id="container-informativos" class="container margin-bottom-100 margin-top-50">
    <div class="row">

        <div class="col-md-12" style="text-align: center;">
            <h3 class="headline centered margin-bottom-35 margin-top-10 headline-box">Informativo</h3>
        </div>

        
        <?php
        while ($informativos->have_posts()) : $informativos->the_post();?>

            <div class="col-md-4">
                    <!-- Blog Post -->
                    <div class="blog-post">

                            <!-- Img -->
                            <a href="<?php the_permalink();?>" class="post-img">
                                    <img src="<?php the_post_thumbnail_url( 'informativos' ); ?>" alt="">
                            </a>

                            <!-- Content -->
                            <div class="post-content">
                                <h3><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h3>
                                    <p><?php echo get_field('informativo_resumo');?></p>

                                    <a href="<?php the_permalink();?>" class="read-more">Leia mais <i class="fa fa-angle-right"></i></a>
                            </div>

                    </div>
                    <!-- Blog Post / End -->
            </div>
        <?php endwhile; ?>

    </div>
</div>

<?php } ?>


