<?php
/*
 * Template Name: Procurar Imóveis
 */


$imoveis = '';

if (! isset ($_GET['pag'])) {
    unset($_SESSION['post_pesquisa']);
}

if (isset ($_POST['act'])) {
    $_SESSION['post_pesquisa'] = $_POST;
    $_GET['pag'] = 1; //iniciar da primeira página
}


if (!isset($post_request)) {
    $post_request = isset ($_SESSION['post_pesquisa']) ? $_SESSION['post_pesquisa'] : null;
}



//Paginação indice = (P-1)*Tp
$limite_pag = $posts_per_page; //10
$atual_pag  = ! isset ($_GET['pag']) ? 1 : $_GET['pag'];
$inicio_pag = ($atual_pag <= 1) ? 1 : $atual_pag;
$inicio_pag = ($inicio_pag -1) * $limite_pag;



cloudimo_buscar_imoveis($imoveis, $post_request, [
    'pagination_start' => $inicio_pag, //0
    'pagination_total' => $limite_pag
]);



//Para mensagem total encontrados.
$total_imoveis = $imoveis->Imoveis->Total;

if ($total_imoveis > 1) {
    $total_msg = 'Encontrado <b>'.$total_imoveis.'</b> Imóveis.';
} else {
    $total_msg = 'Encontrado <b>'.$total_imoveis.'</b> Imóvel.';
}


//Total paginas
$total_pag  = ceil($total_imoveis / $limite_pag);

$id_home = get_id_by_slug('home');
?>


<?php get_header(); ?>


<!-- Search
================================================== -->
<section class="search margin-bottom-50">
<div class="container">
	<div class="row">
		<div class="col-md-12">

			<!-- Title -->
			<h3 class="search-title"><?php echo get_field('home_chamada', $id_home);?></h3>

			<!-- Form -->
                        <form method="post">
                            
			<div class="main-search-box no-shadow">


				<!-- Row With Forms -->
				<div class="row with-forms">

					
					<!-- Property Type -->
					<div class="col-md-3">
                                            <select name="tipo_imovel" data-placeholder="Any Type" class="chosen-select-no-single" >
                                                <option value="">Tipo de imóvel</option>
                                                <?php foreach(cloudimo_xml_get_session('tipo-imoveis')->Tipo as $tipo): ?>
                                                <option value="<?=$tipo->ID?>" <?php if ($post_request['tipo_imovel'] == $tipo->ID) echo 'selected="selected"'; ?>><?=$tipo->Nome?></option>
                                                <?php endforeach;?>
                                            </select>
					</div>
                                        
                                        
                                        <!-- Status -->
					<div class="col-md-3">
                                            <select name="categoria" data-placeholder="Any Status" class="chosen-select-no-single" >	
                                                <option value="locacao" <?php if ($post_request['categoria'] == 'locacao')  echo 'selected="selected"';?> >Para Locar</option>
                                                <option value="venda"   <?php if ($post_request['categoria'] == 'venda')    echo 'selected="selected"';?> >Para Comprar</option>
                                                <option value="todos"   <?php if ($post_request['categoria'] == 'todos')    echo 'selected="selected"';?> >Todos</option>
                                            </select>
					</div>
                                        

					<!-- Main Search Input -->
					<div class="col-md-6">
                                            <div class="main-search-input">
                                                <input name="procurar" type="text" placeholder="Digite um bairro, cidade ou código do imóvel."  <?php if ($post_request['procurar']) echo 'value="'.$post_request['procurar'].'"'; ?>/>
                                                <button type="submit" name="act" value="pesquisar" class="button"><i class="fa fa-search"></i></button>
                                            </div>
					</div>

				</div>
				<!-- Row With Forms / End -->


				<!-- Row With Forms -->
				<div class="row with-forms">


					<!-- Min Price -->
					<div class="col-md-3">
						
						<!-- Select Input -->
						<div class="select-input disabled-first-option">
							<input type="text" name="valor_de" placeholder="Preço Min" data-unit="R$" <?php if ($post_request['valor_de']) echo 'value="'.$post_request['valor_de'].'"'; ?>>
							<select>		
                                                            <option>Preço Min</option>
                                                            <option>1.000</option>
                                                            <option>2.000</option>	
                                                            <option>3.000</option>	
                                                            <option>4.000</option>	
                                                            <option>5.000</option>	
                                                            <option>10.000</option>	
                                                            <option>15.000</option>	
                                                            <option>20.000</option>	
                                                            <option>30.000</option>
                                                            <option>40.000</option>
                                                            <option>50.000</option>
                                                            <option>60.000</option>
                                                            <option>70.000</option>
                                                            <option>80.000</option>
                                                            <option>90.000</option>
                                                            <option>100.000</option>
                                                            <option>110.000</option>
                                                            <option>120.000</option>
                                                            <option>130.000</option>
                                                            <option>140.000</option>
                                                            <option>150.000</option>
							</select>
						</div>
						<!-- Select Input / End -->

					</div>


					<!-- Max Price -->
					<div class="col-md-3">
						
						<!-- Select Input -->
						<div class="select-input disabled-first-option">
							<input type="text" name="valor_ate" placeholder="Preço Máx" data-unit="R$" <?php if ($post_request['valor_ate']) echo 'value="'.$post_request['valor_ate'].'"'; ?> >
							<select>		
                                                            <option>Preço Máx</option>
                                                            <option>1.000</option>
                                                            <option>2.000</option>	
                                                            <option>3.000</option>	
                                                            <option>4.000</option>	
                                                            <option>5.000</option>	
                                                            <option>10.000</option>	
                                                            <option>15.000</option>	
                                                            <option>20.000</option>	
                                                            <option>30.000</option>
                                                            <option>40.000</option>
                                                            <option>50.000</option>
                                                            <option>60.000</option>
                                                            <option>70.000</option>
                                                            <option>80.000</option>
                                                            <option>90.000</option>
                                                            <option>100.000</option>
                                                            <option>110.000</option>
                                                            <option>120.000</option>
                                                            <option>130.000</option>
                                                            <option>140.000</option>
                                                            <option>150.000</option>
							</select>
						</div>
						<!-- Select Input / End -->

					</div>
                                        
                                        
                                        <!-- Min Price -->
					<div class="col-md-3">
						
						<!-- Select Input -->
						<div class="select-input disabled-first-option">
							<input name="area_util_de" type="text" placeholder="Área Min " data-unit="m²" <?php if ($post_request['area_util_de']) echo 'value="'.$post_request['area_util_de'].'"'; ?> >
							<select>	
                                                            <option value="">Área Min </option>
                                                            <option>300</option>
                                                            <option>400</option>
                                                            <option>500</option>
                                                            <option>700</option>
                                                            <option>800</option>
                                                            <option>1000</option>
                                                            <option>1500</option>
							</select>
						</div>
						<!-- Select Input / End -->

					</div>

					<!-- Max Price -->
					<div class="col-md-3">
						
						<!-- Select Input -->
						<div class="select-input disabled-first-option">
							<input name="area_util_ate" type="text" placeholder="Área Máx" data-unit="m²" <?php if ($post_request['area_util_ate']) echo 'value="'.$post_request['area_util_ate'].'"'; ?>>
							<select>	
                                                            <option value="">Área Máx</option>
                                                            <option>300</option>
                                                            <option>400</option>
                                                            <option>500</option>
                                                            <option>700</option>
                                                            <option>800</option>
                                                            <option>1000</option>
                                                            <option>1500</option>
							</select>
						</div>
						<!-- Select Input / End -->

					</div>

				</div>
				<!-- Row With Forms / End -->


				<!-- More Search Options -->
				<a href="#" class="more-search-options-trigger margin-top-10" data-open-title="Mais opçoes" data-close-title="Ocultar mais opçoes"></a>

				<div class="more-search-options relative">
					<div class="more-search-options-container">

						<!-- Row With Forms -->
						<div class="row with-forms">

							<!-- Age of Home -->
							<div class="col-md-3">
                                                            <select name="garagem" data-placeholder="Garagens" class="chosen-select-no-single" >
                                                                <option label="blank" value=""></option>	
                                                                <option value="">Indefinido</option>	
                                                                <option>1</option>
                                                                <option>2</option>
                                                                <option>3</option>
                                                                <option>4</option>
                                                                <option>5</option>
                                                            </select>
							</div>

							<!-- Rooms Area -->
							<div class="col-md-3">
                                                            <select name="salas" data-placeholder="Salas" class="chosen-select-no-single" >
                                                                <option label="blank" value=""></option>	
                                                                <option value="">Indefinido</option>	
                                                                <option>1</option>
                                                                <option>2</option>
                                                                <option>3</option>
                                                                <option>4</option>
                                                                <option>5</option>
                                                            </select>
							</div>

							<!-- Quartos -->
							<div class="col-md-3">
                                                            <select name="quartos" data-placeholder="Quartos" class="chosen-select-no-single" >
                                                                <option label="blank" value=""></option>	
                                                                <option value="">Indefinido</option>	
                                                                <option>1</option>
                                                                <option>2</option>
                                                                <option>3</option>
                                                                <option>4</option>
                                                                <option>5</option>
                                                            </select>
							</div>

							<!-- Banheiros -->
							<div class="col-md-3">
                                                            <select name="banheiros" data-placeholder="Banheiros" class="chosen-select-no-single" >
                                                                <option label="blank" value=""></option>	
                                                                <option value="">Indefinido</option>	
                                                                <option>1</option>
                                                                <option>2</option>
                                                                <option>3</option>
                                                                <option>4</option>
                                                                <option>5</option>
                                                            </select>
							</div>

						</div>
						<!-- Row With Forms / End -->


						<!-- Checkboxes --
						<div class="checkboxes in-row">
					
							<input id="check-2" type="checkbox" name="check">
							<label for="check-2">Air Conditioning</label>

							<input id="check-3" type="checkbox" name="check">
							<label for="check-3">Swimming Pool</label>

							<input id="check-4" type="checkbox" name="check" >
							<label for="check-4">Central Heating</label>

							<input id="check-5" type="checkbox" name="check">
							<label for="check-5">Laundry Room</label>	


							<input id="check-6" type="checkbox" name="check">
							<label for="check-6">Gym</label>

							<input id="check-7" type="checkbox" name="check">
							<label for="check-7">Alarm</label>

							<input id="check-8" type="checkbox" name="check">
							<label for="check-8">Window Covering</label>
					
						</div>
						<! -- Checkboxes / End -->
                                                

					</div>

				</div>
				<!-- More Search Options / End -->


			</div>
			<!-- Box / End -->
                        </form>
                        
		</div>
	</div>
</div>
</section>



<!-- Content
================================================== -->
<div class="container">
    <div class="row fullwidth-layout">

            <div class="col-md-12">

                    <!-- Sorting / Layout Switcher -->
                    <div class="row margin-bottom-15">

                            <div class="col-md-6">
                                <label><?=$total_msg?></label>
                                    <div class="sort-by">

                                            <!--
                                            <label>Sort by:</label>

                                            <div class="sort-by-select">
                                                    <select data-placeholder="Default order" class="chosen-select-no-single" >
                                                            <option>Default Order</option>	
                                                            <option>Price Low to High</option>
                                                            <option>Price High to Low</option>
                                                            <option>Newest Properties</option>
                                                            <option>Oldest Properties</option>
                                                    </select>
                                            </div>
                                            -->
                                    </div>
                            </div>

                            <div class="col-md-6">
                                    <!-- Layout Switcher -->
                                    <div class="layout-switcher">
                                            <a href="#" class="grid-three"><i class="fa fa-th"></i></a>
                                            <a href="#" class="grid"><i class="fa fa-th-large"></i></a>
                                            <a href="#" class="list"><i class="fa fa-th-list"></i></a>
                                    </div>
                            </div>
                    </div>


                    <!-- Listings -->
                    <div class="listings-container grid-layout-three">



                        
                        <?php foreach ($imoveis->Imoveis->Imovel as $key => $imovel): ?>
                        
                            <!-- Listing Item -->
                            <div class="listing-item">

                                    <a href="<?=get_page_link(8)?>?id=<?=$imovel->ID?>" class="listing-img-container">

                                        <div class="listing-badges">
                                                <?php $tarja = (string) $imovel->Tarja; if($tarja): ?>
                                                <span class="featured"><?=$tarja?></span>
                                                <?php endif; ?>
                                                <span><?=$imovel->CategoriaImovel?></span>
                                        </div>

                                        <div class="listing-img-content">                                                    

                                            <?php if ($imovel->CategoriaImovel == 'Venda/Locação'): ?>
                                            <span class="listing-price">Venda R$ <?=$imovel->PrecoVenda?></i></span>
                                            <span class="listing-price">Locação R$ <?=$imovel->PrecoLocacao?></i></span>
                                            <?php endif; ?>

                                            <?php if ($imovel->CategoriaImovel == 'Locação'): ?>
                                            <span class="listing-price">R$ <?=$imovel->PrecoLocacao?></i></span>
                                            <?php endif; ?>

                                            <?php if ($imovel->CategoriaImovel == 'Venda'): ?>
                                            <span class="listing-price">R$ <?=$imovel->PrecoVenda?></i></span>
                                            <?php endif; ?>

                                            <!-- <span class="like-icon tooltip"></span>-->

                                        </div>

                                        <img src="<?=cloudimo_get_image($imovel->Fotos->Foto->URLArquivo)?>" alt="">

                                        <!--
                                        <div class="listing-carousel">
                                            <div><img src="< ?php echo get_template_directory_uri(); ?>/images/listing-01.jpg" alt=""></div>
                                            <div><img src="< ?php echo get_template_directory_uri(); ?>/images/listing-01b.jpg" alt=""></div>
                                            <div><img src="< ?php echo get_template_directory_uri(); ?>/images/listing-01c.jpg" alt=""></div>
                                        </div>
                                        -->
                                    </a>

                                    <div class="listing-content">

                                        <div class="listing-title">
                                                <h6>Código: <?php echo $imovel->CodigoImovel; ?></h6>
                                                <h4><a href="<?=get_page_link(8)?>?id=<?=$imovel->ID?>"><?php echo $imovel->TipoImovel; ?></a></h4>
                                                <a href="#" class="listing-address popup-gmaps">
                                                        <i class="fa fa-map-marker"></i>
                                                        <?php echo $imovel->Bairro . ' - ' . $imovel->Cidade . ' - ' . $imovel->Uf; ?>
                                                </a>

                                                <a href="<?=get_page_link(8)?>?id=<?=$imovel->ID?>" class="details button border">Mais detalhes</a>
                                        </div>

                                        <ul class="listing-details">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <?php if ((int) $imovel->QtdSalas) echo "<li><i class='fa fa-home'></i>  {$imovel->QtdSalas} Sala</li>"; ?>
                                                    <?php if ((int) $imovel->QtdSuites) echo "<li><i class='fa fa-bed'></i>  {$imovel->QtdSuites} Suíte</li>"; ?>
                                                    <?php if ((int) $imovel->QtdDormitorios) echo "<li><i class='fa fa-bed'></i>  {$imovel->QtdDormitorios} Quarto</li>"; ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php if ((int) $imovel->QtdBanheiros) echo "<li><i class='fa fa-bath'></i>  {$imovel->QtdBanheiros} Banheiro</li>"; ?>
                                                    <?php if ((int) $imovel->QtdVagasGaragem) echo "<li><i class='fa fa-car'></i>  {$imovel->QtdVagasGaragem} Garagem</li>"; ?>
                                                </div>
                                            </div>
                                        </ul>

                                        <div class="listing-footer">
                                            <i class="fa fa-user"></i>
                                                <?php
                                                foreach(cloudimo_xml_get_session('usuarios')->Usuario as $usuario){
                                                    if($usuario->ID == $imovel->IDUsuario) {
                                                        echo $usuario->Nome; break;
                                                    }
                                                }                                                       
                                                ?>
                                                <!--<span><i class="fa fa-calendar-o"></i> <?=$imovel->DataResumo?></span>-->
                                        </div>
                                    </div>

                            </div>
                            <!-- Listing Item / End -->
                            
                        <?php endforeach; ?>

                    </div>
                    <!-- Listings Container / End -->
                    
                    
                    <div class="clearfix"></div>

                    <!-- Pagination -->
                    <div class="pagination-container margin-top-20 margin-bottom-50">
                        <?=cloudimo_html_pagination($atual_pag, $total_pag)?>
                    </div>
                    <!-- Pagination / End -->
                    
            </div>
        
    </div>
    
</div>



<?php get_footer();?>
