<?php
$id_home = get_id_by_slug('home');
$logo_home = get_field('home_logo', $id_home);

//Capturando email para contato
$email = get_bloginfo('admin_email');
if (get_field('home_email', $id_home)) {
    $email = get_field('home_email', $id_home);
}

?>

<!-- Footer
================================================== -->
<div id="footer" class="sticky-footer">
	<!-- Main -->
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-6">
                            <img class="footer-logo" src="<?php echo $logo_home['sizes']['home_logo']; ?>" alt="<?=get_bloginfo('blogname')?>">
				<br><br>
				<p><?=get_field('home_resumo_sobre_nos', $id_home)?></p>
			</div>

                        <?php
                        
                            $servicos = new WP_Query([
                                'post_type' => 'servicos'
                            ]);

                            $id_home = get_id_by_slug('home');

                            if($servicos->have_posts()):
                        ?>
			<div class="col-md-3 col-sm-6 ">
				<h4>Mais páginas</h4>
				<ul class="footer-links">
                                    <li><a href="<?php echo get_permalink( get_id_by_slug('institucional') ); ?>">Institucional</a></li>
                                    <li><a href="<?php echo get_permalink( get_id_by_slug('procurar') ); ?>">Procurar Imóveis</a></li>
                                    <li><a href="<?php echo get_permalink( get_id_by_slug('contato') ); ?>">Contato</a></li>
                                    <?php
                                    while ($servicos->have_posts()) : $servicos->the_post();
                                        
                                        $link_servico = get_field('servico_link');
                                        
                                        if (! $link_servico) {
                                            $link_servico = get_permalink();
                                        } ?>                             
                                        <li><a href="<?=$link_servico?>"><?=get_field('servico_titulo')?></a></li>
                                    <?php endwhile; ?>
                                    
				</ul>
				<div class="clearfix"></div>
                        </div>
                        <?php endif; ?>

			<div class="col-md-4  col-sm-12">
				<h4>Fale conosco</h4>                                
				<div class="text-widget">
					<span><?=get_field('home_onde_estamos', $id_home)?></span> <br><br>
					Telefone: <span><?=get_field('home_telefone', $id_home)?></span><br>
					E-Mail:<span> <a href="#"><?=$email?></a> </span><br>
				</div>



			</div>

		</div>
		
		<!-- Copyright -->
		<div class="row">
			<div class="col-md-12">
                            <div class="copyrights">
                                © Copyright <?php echo date ('Y').' - '.get_field('home_copyright', $id_home)?>
                                <div id="powered-by">
                                    <a href="http://www.cloudimo.com.br" title="Desenvolvido por: Cloudimo - Gestor Imobiliário">
                                        <span>Cloudimo - Gestor Imobiliário</span>
                                    </a>
                                </div>
                            </div>
			</div>
		</div>

	</div>

</div>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div> 


<!-- Scripts
================================================== -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/chosen.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/magnific-popup.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/rangeSlider.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/sticky-kit.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/slick.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/masonry.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/jquery.jpanelmenu.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/tooltips.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/custom.js"></script>


<!-- Style Switcher
================================================== --
<script src="php echo get_template_directory_uri(); ?>/scripts/switcher.js"></script>

<div id="style-switcher" style="display:none;">
	<h2>Color Switcher <a href="#"><i class="sl sl-icon-settings"></i></a></h2>
	
	<div>
		<ul class="colors" id="color1">
			<li><a href="#" class="blue" title="Blue"></a></li>
			<li><a href="#" class="green" title="Green"></a></li>
			<li><a href="#" class="orange" title="Orange"></a></li>
			<li><a href="#" class="navy" title="Navy"></a></li>
			<li><a href="#" class="yellow" title="Yellow"></a></li>
			<li><a href="#" class="peach" title="Peach"></a></li>
			<li><a href="#" class="beige" title="Beige"></a></li>
			<li><a href="#" class="purple" title="Purple"></a></li>
			<li><a href="#" class="celadon" title="Celadon"></a></li>
			<li><a href="#" class="pink" title="Pink"></a></li>
			<li><a href="#" class="red" title="Red"></a></li>
			<li><a href="#" class="brown" title="Brown"></a></li>
			<li><a href="#" class="cherry" title="Cherry"></a></li>
			<li><a href="#" class="cyan" title="Cyan"></a></li>
			<li><a href="#" class="gray" title="Gray"></a></li>
			<li><a href="#" class="olive" title="Olive"></a></li>
		</ul>
	</div>
		
</div>
< !-- Style Switcher / End -->


</div>
<!-- Wrapper / End -->


</body>
</html>
