<?php
get_header();
?>

<!-- Titlebar
================================================== -->
<div id="titlebar">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php the_title(); ?></h2>
            </div>
        </div>
    </div>
</div>



<!-- Content
================================================== -->
<div class="container">

    <!-- Blog Posts -->
    <div class="blog-page">
        <div class="row">


            <!-- Post Content -->
            <div class="col-md-12">


                <!-- Blog Post -->
                <div class="blog-post single-post">

                    <!-- Content -->
                    <div class="post-content">
                        
                        <?php
			// Start the Loop.
			while ( have_posts() ) : the_post();
                            the_content();
			// End the loop.
			endwhile;			
                        ?>
                        

                        <!-- Share Buttons -->
                        <ul class="share-buttons margin-top-40 margin-bottom-0">
                            <li><a class="fb-share" href="#"><i class="fa fa-facebook"></i> Share</a></li>
                            <li><a class="twitter-share" href="#"><i class="fa fa-twitter"></i> Tweet</a></li>
                            <li><a class="gplus-share" href="#"><i class="fa fa-google-plus"></i> Share</a></li>
                            <li><a class="pinterest-share" href="#"><i class="fa fa-pinterest-p"></i> Pin</a></li>
                        </ul>
                        <div class="clearfix"></div>

                    </div>
                </div>
                <!-- Blog Post / End -->

            </div>
            <!-- Content / End -->


           
        </div>

    </div>
</div>

<?php
get_footer();
?>
