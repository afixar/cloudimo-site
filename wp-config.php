<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa user o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'cloudimo_site');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'afixar');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ftwgyU_[RWddu-ym%iogZfU2R2(kJzqn+weGYR-[dd [8?Fr.QfG4(|~2MZ6vaq~');
define('SECURE_AUTH_KEY',  '3Ve`J!rJ/a_%iA.1eU8+(zJ]$lv*W|U.rMb#$:X AoHrQ7cB8[hf?hez=OXoOxA!');
define('LOGGED_IN_KEY',    '*{f{A|S3L$x~N4To,]=]H 3:y$|%+VMv(Xx-7Y+R5>;19aMe}*Vz[ZNox!gL=9+j');
define('NONCE_KEY',        'y<y.FPUF;/RSb@l]2Q-}-c/a6{CRl)&K7v>:M8]kF^fqv*N($x3}7jt}>@)MH+X=');
define('AUTH_SALT',        'cjq<AoVcGtvn2=#@VVe?]Z4B2}U{4soemu2&{g1u{uMD_qrbfT@eL>P3:[>6Ch<2');
define('SECURE_AUTH_SALT', '&Jxt)E58%3^KLpE,@B$S:zpZ$ct,v<?c95tu.E7LW$>pia7c-(&=AIo6jbu($Dju');
define('LOGGED_IN_SALT',   ']9zsdu`5sYV!<Jp]L(Q.yjSd LCK9+bS/)[RM}oGbt#09}UEMD)AkQ?A9q|mtUJQ');
define('NONCE_SALT',       'O~?_)Z^qQf9=dA fgO5?Bxy|CHgnoDi*P(>.WZG*2v(;nI{Y{OpVe|VLQ2c@GVO{');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * para cada um um único prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'afx_wp_';

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
